#include <iostream>
#include <cmath>
#include"holstein.hpp"
#include<vector>
using namespace std;
using namespace itensor;

int main(int argc, char *argv[])
{
  int L=4;
  int M=3;
  {
    auto sites=Holstein(L, {"ConserveNf", true, "MaxOcc", M });
    Print(sites);
  }
    {
        std::vector<int> v(L, M);
   v[0]=5;
   v[1]=5; 
  
    auto sites=Holstein(L, {"ConserveNf", true, "DiffMaxOcc=",true, "MaxOccVec=", v});
    Print(sites);
  }
    return(0);
}
