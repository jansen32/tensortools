#include <iostream>
#include <cmath>
#include"../include/vidalnot.hpp"
#include"../include/holstein.hpp"
using namespace std;
using namespace itensor;
int main(int argc, char *argv[])
{
  int N=3;
  int M=0;
    std::vector<int> v(N, 0);
    for(int i=0; i<N; i++)
    {
      v[i]=M;
      //std::cout << " i "<< i<<std::endl;
    }

   // auto argsState = itensor::Args({"ConserveNf=",true,
   //                           "ConserveNb=",false,
   //    "DiffMaxOcc=",true, "MaxOccVec=", v} );
      auto argsState = itensor::Args({"ConserveNf=",true,
                             "ConserveNb=",false,
       "MaxOcc=", M} );
    auto sites = itensor::Holstein(N, argsState);
        auto state = itensor::InitState(sites);
    // for(int i=1; i<=N; i+=1)
    //   {
    // 	if(i%2==0){state.set(i, "Occ");}
    //   }
 state.set(1, "Occ");
     auto psi = MPS(state);
     psi.position(1);
     psi.normalize();
     auto H1=itensor::makeHolstHam(sites, 1, 1, 1);
    	 auto sweeps = itensor::Sweeps(20);
    //very important to use noise for this model
    sweeps.noise() = 1E-6,1E-6,1E-8, 1E-10,  1E-12;
    sweeps.maxdim() = 10,20,100,100,100;
    sweeps.cutoff() = 1E-14;
    std::cout<< "start "<< std::endl;
     auto [energy,psi0] = itensor::dmrg(H1,psi,sweeps,{"Quiet=",true});
        itensor::printfln("Ground State Energy1 = %.12f",energy);
  psi0.position(1);
  psi0.normalize();
  std::cout<< "norm "<< innerC(psi0,H1, psi0)<<std::endl;
    auto psiv=itensor::vMPS(psi0);
//    //   auto g=psi;
//         // g.position(1);
// 	// g.normalize();
    std::cout<< "norm "<<innerC(psi0, H1, psi0)<<std::endl;
    return(0);
}
