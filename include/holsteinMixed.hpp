//
// Distributed under the ITensor Library License, Version 1.2
//    (See accompanying LICENSE file.)
//
 #ifndef HOLSTEINMIXED_H
#define HOLSTEINMIXED_H
#include "itensor/mps/siteset.h"
#include "itensor/all.h"
#include<type_traits>
#include"extra.hpp"
namespace itensor {
using HolsteinMixed = MixedSiteSet<FermionSite,BosonSite>;
  template<>
  struct is_Mixed<HolsteinMixed>: std::true_type {
   
  };
      
      template<typename siteset>
  auto makeHolstHam(siteset sites, double t0=1, double gamma=1, double omega=1)
	->std::enable_if_t<is_Mixed<siteset>::value, AutoMPO>
      {
    std::cout<< "used mixed "<< std::endl;
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  <= N-2; j+=2)
        {

      	ampo += -t0,"Cdag",j,"C",j+2;
	ampo+= -t0, "C",j,"Cdag",j+2 ;

    }
    for(int j=1;j < N; j += 2)
        {

	   ampo += gamma,"N",j,"A",j+1;
	  ampo += gamma,"N",j,"Adag",j+1;
        }
    for(int j = 1; j <= N; j += 2)
        {
	   ampo += omega,"N",j+1;

        }
    
    return ampo;
  }
  template<typename siteset>
  auto makePureHolstHam(siteset sites, double t0=1, double gamma=1, double omega=1)
    	->std::enable_if_t<is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
    std::cout<< " N  " << N << std::endl;
        auto ampo = AutoMPO(sites);
    	    for(int j=1;j  <= N-4; j+=4)
        {


      	ampo += -t0,"Cdag",j,"C",j+4;
    	ampo+= -t0, "C",j,"Cdag",j+4 ;

    }
    for(int j=1;j < N; j += 4)
        {
    	   ampo += gamma,"N",j,"A",j+1;
    	  ampo += gamma,"N",j,"Adag",j+1;
        }
    for(int j = 1; j < N; j += 4)
        {

		   ampo += omega,"N",j+1;

        }
    
    return ampo;
  }
  

} //namespace itensor
#endif /* HOLSTEINMIXED_H */
