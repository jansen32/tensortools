#ifndef MAKEHAMILTONIANS_H
#define MAKEHAMILTONIANS_H
#include"itensor/all.h"
 namespace itensor{
	
  template<typename Lattice>
  MPO Nph(const Lattice& sites, double omega=1.)
  {
      auto ampo = AutoMPO(sites);

      for(int b = 1; b <= length(sites); b+=1)
     {
       ampo += omega,"Nph",b;  
      }
        auto O=toMPO(ampo);
	return O;
  }
  template<typename Lattice>
  MPO Nph_bath(const Lattice& sites, double omega=1.)
  {
      auto ampo = AutoMPO(sites);

      for(int b = 1; b <= length(sites); b+=2)
     {
       ampo += omega,"Nph",b;  
      }
        auto O=toMPO(ampo);
	return O;
  }
   	
  template<typename Lattice>
  MPO Ne(const Lattice& sites, double omega=1.)
  {
      auto ampo = AutoMPO(sites);

      for(int b = 1; b <= length(sites); b+=1)
     {
       ampo += omega,"N",b;  
      }
        auto O=toMPO(ampo);
	return O;
  }
     template<typename Lattice>
     MPO Nphat(const Lattice& sites, int i, double omega=1.)
  {
      auto ampo = AutoMPO(sites);


       ampo += omega,"N",i;  
      
        auto O=toMPO(ampo);
	return O;
  }
     template<typename Lattice>
  MPO Ekin(const Lattice& sites, double t0=1.)
  {
      auto ampo = AutoMPO(sites);

      for(int b = 1; b <= length(sites)-2; b+=2)
     {
	      	ampo += -t0,"Cdag",b,"C",b+2;
		ampo += -t0, "Cdag",b+2,"Cdag",b;
      }
        auto O=toMPO(ampo);
	return O;
  }
  }
#endif /* MAKEHAMILTONIANS_H */
