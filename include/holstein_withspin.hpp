
//
// Distributed under the ITensor Library License, Version 1.2
//    (See accompanying LICENSE file.)
//
 #ifndef HOLSTEIN_WITHSPIN_H
#define HOLSTEIN_WITHSPIN_H
#include "itensor/mps/siteset.h"
#include "itensor/all.h"
#include"extra.hpp"
namespace itensor {
 template<typename SiteSet>
void applyCdagup(MPS& psi, int i, SiteSet& sites)
{
   psi.position(i);
        psi.Aref(i)*= op(sites,"Adagup",i);
	psi.Aref(i).noPrime("Site");
     for(int k = i-1; k >0; --k)
    {
      psi.position(k);
      	psi.Aref(k) *= op(sites,"F",k);
      	psi.Aref(k).noPrime("Site");
     }
  
}

  template<typename SiteSet>
void applyCdagdn(MPS& psi, int i, SiteSet& sites)
{
   psi.position(i);
        psi.Aref(i)*= op(sites,"Adagdn",i);
	psi.Aref(i).noPrime("Site");
     for(int k = i-1; k >0; --k)
    {
      psi.position(k);
      	psi.Aref(k) *= op(sites,"F",k);
      	psi.Aref(k).noPrime("Site");
     }
  
}
void applyCup(MPS& psi, int i, SiteSet& sites)
{
   psi.position(i);
        psi.Aref(i)*= op(sites,"Aup",i);
	psi.Aref(i).noPrime("Site");
     for(int k = i-1; k >0; --k)
    {
      psi.position(k);
      	psi.Aref(k) *= op(sites,"F",k);
      	psi.Aref(k).noPrime("Site");
     }
  
}

  template<typename SiteSet>
void applyCdn(MPS& psi, int i, SiteSet& sites)
{
   psi.position(i);
        psi.Aref(i)*= op(sites,"Adn",i);
	psi.Aref(i).noPrime("Site");
     for(int k = i-1; k >0; --k)
    {
      psi.position(k);
      	psi.Aref(k) *= op(sites,"F",k);
      	psi.Aref(k).noPrime("Site");
     }
  
}

  
  template<typename siteset>
auto makeCurr_withspin(siteset sites, double t0=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N; j+=1)
        {

      	ampo += t0*Cplx_i,"Cdagup",j,"Cup",j+1;
	ampo+= -t0*Cplx_i, "Cdagup",j+1,"Cup",j ;
      	ampo += t0*Cplx_i,"Cdagdn",j,"Cdn",j+1;
	ampo+= -t0*Cplx_i, "Cdagdn",j+1,"Cdn",j ;
	}
    

    return ampo;
  }

  template<typename siteset>
auto makeEK_withspin(siteset sites, double t0=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N; j+=1)
        {

      	ampo += -t0,"Cdagup",j,"Cup",j+1;
	ampo+= -t0, "Cdagup",j+1,"Cup",j ;
      	ampo += -t0,"Cdagdn",j,"Cdn",j+1;
	ampo+= -t0, "Cdagdn",j+1,"Cdn",j ;
	}
    

    return ampo;
  }
    template<typename siteset>
auto makeCurr_FT_withspin(siteset sites, double t0=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N-2; j+=2)
        {

	  ampo += t0*Cplx_i,"Cdagup",j,"Cup",j+2;
	   	ampo+= -t0*Cplx_i, "Cdagup",j+2,"Cup",j ;
	  ampo += t0*Cplx_i,"Cdagdn",j,"Cdn",j+2;
	   	ampo+= -t0*Cplx_i, "Cdagdn",j+2,"Cdn",j ;

	}
    

    return ampo;
  }
    template<typename siteset>
auto makeEK_FT_withspin(siteset sites, double t0=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N-2; j+=2)
        {
      	ampo += -t0,"Cdagup",j,"Cup",j+2;
	ampo+= -t0, "Cdagup",j+2,"Cup",j ;
      	ampo += -t0,"Cdagdn",j,"Cdn",j+2;
	ampo+= -t0, "Cdagdn",j+2,"Cdn",j ;
	}
    

    return ampo;
  }

  template<typename siteset>
  void get_CurCorr_momentum1_left_FT_withspin(siteset sites,AutoMPO& ampo,std::string spin, double t0=1, double gamma=1 )
  {
int N=length(sites);
	    for(int j=1;j  <= N-3; j+=2)
        {

	  ampo += gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+2, "B",j;
	  ampo+= gamma*t0*Cplx_i, "Cdag"+spin,j+2,"C"+spin,j, "B",j ;
	  ampo += gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+2, "Bdag",j;
	  ampo+= gamma*t0*Cplx_i, "Cdag"+spin,j+2,"C"+spin,j, "Bdag",j ;
	}
    	    for(int j=3;j  <= N-1; j+=2)
        {

	  ampo += -gamma*t0*Cplx_i,"Cdag"+spin,j-2,"C"+spin,j, "B",j;
	  ampo+= -gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-2, "B",j ;
	  ampo +=-gamma*t0*Cplx_i,"Cdag"+spin,j-2,"C"+spin,j, "Bdag",j;
	  ampo+= -gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-2, "Bdag",j ;
	}
	  
	     ampo+= 2*t0*t0*Cplx_i, "Cdag"+spin,1,"C"+spin,1;
	    	  ampo+= -2*t0*t0*Cplx_i, "Cdag"+spin,N-1,"C"+spin,N-1;
		  return;
}  
template<typename siteset>
  auto CurCorr_momentum1_left_FT_withspin(siteset sites, double t0=1, double gamma=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
    
        auto ampo = AutoMPO(sites);
	get_CurCorr_momentum1_left_FT_withspin(sites,ampo,"up", t0,gamma);
	get_CurCorr_momentum1_left_FT_withspin(sites,ampo,"dn", t0,gamma);
    return ampo;
  }
template<typename siteset>
  void get_CurCorr_momentum2_left_FT_withspin(siteset sites,  AutoMPO& ampo, std::string spin,  double t0, double gamma, double omega, double omegap)
  {
    int N=length(sites);
	    for(int j=3;j  <= N-1; j+=2)
        {

	  ampo += omega*gamma*t0*Cplx_i,"Cdag"+spin,j-2,"C"+spin,j, "B",j;
	  ampo+= omega*gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-2, "B",j ;
	  ampo += -omega*gamma*t0*Cplx_i,"Cdag"+spin,j-2,"C"+spin,j, "Bdag",j;
	  ampo+= -omega*gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-2, "Bdag",j ;
	}
   	    for(int j=1;j  < N-1; j+=2)
        {

   	  ampo += -omega*gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+2, "B",j;
   	  ampo+= -omega*gamma*t0*Cplx_i, "Cdag"+spin,j+2,"C"+spin,j, "B",j;
   	  ampo += omega*gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+2, "Bdag",j;
   	  ampo+= omega*gamma*t0*Cplx_i, "Cdag"+spin,j+2,"C"+spin,j, "Bdag",j ;
   	}
    	 
   	    for(int j=1;j  < N-1; j+=2)
        {

   	  ampo += -gamma*gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+2, "X",j, "X",j+2;
   	  ampo+= -gamma*gamma*t0*Cplx_i, "Cdag"+spin,j+2,"C"+spin,j, "X",j, "X",j;
   	  ampo += gamma*gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+2, "X",j, "X",j;
   	  ampo+= gamma*gamma*t0*Cplx_i, "Cdag"+spin,j+2,"C"+spin,j, "X",j, "X",j+2 ;
   	}
   for(int j=3;j  <= N-1; j+=2)
        {

   	  ampo += gamma*gamma*t0*Cplx_i,"Cdag"+spin,j-2,"C"+spin,j, "X",j, "X",j;
   	  ampo+= gamma*gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-2, "X",j, "X",j-2;
   	  ampo += -gamma*gamma*t0*Cplx_i,"Cdag"+spin,j-2,"C"+spin,j, "X",j, "X",j-2;
   	  ampo+= -gamma*gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-2, "X",j, "X",j ;
   	}

   // double check this sign
	    for(int j=1;j  < N-3; j+=2)
        {

   	  ampo += -gamma*t0*t0*Cplx_i,"Cdag"+spin,j+4,"C"+spin,j, "X",j;
   	  ampo+= gamma*t0*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j+4, "X",j;
   	  ampo += -gamma*t0*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+4, "X",j+2;
   	  ampo+= gamma*t0*t0*Cplx_i, "Cdag"+spin,j+4,"C"+spin,j, "X",j+2 ;
   	}
   for(int j=3;j  < N-1; j+=2)
        {

   	  ampo += -gamma*t0*t0*Cplx_i,"Cdag"+spin,j-2,"C"+spin,j+2, "X",j;
   	  ampo+= gamma*t0*t0*Cplx_i, "Cdag"+spin,j+2,"C"+spin,j-2, "X",j;
   	  ampo += -gamma*t0*t0*Cplx_i,"Cdag"+spin,j+2,"C"+spin,j-2, "X",j+2;
   	  ampo+= gamma*t0*t0*Cplx_i, "Cdag"+spin,j-2,"C"+spin,j+2, "X",j+2 ;
   	}
    	    for(int j=3;j  < N-1; j+=1)
        {

   	  ampo += omegap*gamma*t0*Cplx_i,"Cdag"+spin,j-2,"C"+spin,j, "B",j+2;
   	  ampo+= omegap*gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-2, "B",j+2 ;
   	  ampo += -omegap*gamma*t0*Cplx_i,"Cdag"+spin,j-2,"C"+spin,j, "Bdag",j+2;
   	  ampo+= -omegap*gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-2, "Bdag",j+2 ;

   	  ampo += omegap*gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+2, "B",j;
   	  ampo+= omegap*gamma*t0*Cplx_i, "Cdag"+spin,j+2,"C"+spin,j, "B",j;
   	  ampo += -omegap*gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+2, "Bdag",j;
   	  ampo+= -omegap*gamma*t0*Cplx_i, "Cdag"+spin,j+2,"C"+spin,j, "Bdag",j ;
   	}


   	    ampo+= 2*t0*t0*t0*Cplx_i, "Cdag"+spin,1,"C"+spin,3;
   	    ampo+= -2*t0*t0*t0*Cplx_i, "Cdag"+spin,3,"C"+spin,1;
   	    ampo+= 2*t0*t0*t0*Cplx_i, "Cdag"+spin,N-3,"C"+spin,N-1;
   	    ampo+= -2*t0*t0*t0*Cplx_i, "Cdag"+spin,N-1,"C"+spin,N-3;
}
   template<typename siteset>
   auto CurCorr_momentum2_left_FT_withspin(siteset sites, double t0, double gamma, double omega, double omegap)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	
	get_CurCorr_momentum2_left_FT_withspin( sites,   ampo, "dn",   t0, gamma,  omega, omegap);
	get_CurCorr_momentum2_left_FT_withspin( sites,   ampo, "up",   t0, gamma,  omega, omegap);
    return ampo;
  }

   template<typename siteset>
   void get_CurCorr_momentum2_left_withspin(siteset sites,AutoMPO& ampo, std::string spin, double t0, double gamma, double omega, double omegap)
   {
    int N=length(sites);
 for(int j=2;j  <= N; j+=1)
        {

	  ampo += omega*gamma*t0*Cplx_i,"Cdag"+spin,j-1,"C"+spin,j, "B",j;
	  ampo+= omega*gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-1, "B",j ;
	  ampo += -omega*gamma*t0*Cplx_i,"Cdag"+spin,j-1,"C"+spin,j, "Bdag",j;
	  ampo+= -omega*gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-1, "Bdag",j ;
	}
	    for(int j=1;j  < N; j+=1)
        {

	  ampo += -omega*gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+1, "B",j;
	  ampo+= -omega*gamma*t0*Cplx_i, "Cdag"+spin,j+1,"C"+spin,j, "B",j;
	  ampo += omega*gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+1, "Bdag",j;
	  ampo+= omega*gamma*t0*Cplx_i, "Cdag"+spin,j+1,"C"+spin,j, "Bdag",j ;
	}
    	 
	    for(int j=1;j  < N; j+=1)
        {

	  ampo += -gamma*gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+1, "X",j, "X",j+1;
	  ampo+= -gamma*gamma*t0*Cplx_i, "Cdag"+spin,j+1,"C"+spin,j, "X",j, "X",j;
	  ampo += gamma*gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+1, "X",j, "X",j;
	  ampo+= gamma*gamma*t0*Cplx_i, "Cdag"+spin,j+1,"C"+spin,j, "X",j, "X",j+1 ;
	}
   for(int j=2;j  <= N; j+=1)
        {

	  ampo += gamma*gamma*t0*Cplx_i,"Cdag"+spin,j-1,"C"+spin,j, "X",j, "X",j;
	  ampo+= gamma*gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-1, "X",j, "X",j-1;
	  ampo += -gamma*gamma*t0*Cplx_i,"Cdag"+spin,j-1,"C"+spin,j, "X",j, "X",j-1;
	  ampo+= -gamma*gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-1, "X",j, "X",j ;
	}

   // double check this sign
	    for(int j=1;j  < N-1; j+=1)
        {

	  ampo += -gamma*t0*t0*Cplx_i,"Cdag"+spin,j+2,"C"+spin,j, "X",j;
	  ampo+= gamma*t0*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j+2, "X",j;
	  ampo += -gamma*t0*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+2, "X",j+1;
	  ampo+= gamma*t0*t0*Cplx_i, "Cdag"+spin,j+2,"C"+spin,j, "X",j+1 ;
	}
   for(int j=2;j  < N; j+=1)
        {

	  ampo += -gamma*t0*t0*Cplx_i,"Cdag"+spin,j-1,"C"+spin,j+1, "X",j;
	  ampo+= gamma*t0*t0*Cplx_i, "Cdag"+spin,j+1,"C"+spin,j-1, "X",j;
	  ampo += -gamma*t0*t0*Cplx_i,"Cdag"+spin,j+1,"C"+spin,j-1, "X",j+1;
	  ampo+= gamma*t0*t0*Cplx_i, "Cdag"+spin,j-1,"C"+spin,j+1, "X",j+1 ;
	}
    	    for(int j=2;j  < N; j+=1)
        {

	  ampo += omegap*gamma*t0*Cplx_i,"Cdag"+spin,j-1,"C"+spin,j, "B",j+1;
	  ampo+= omegap*gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-1, "B",j+1 ;
	  ampo += -omegap*gamma*t0*Cplx_i,"Cdag"+spin,j-1,"C"+spin,j, "Bdag",j+1;
	  ampo+= -omegap*gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-1, "Bdag",j+1 ;

	  ampo += omegap*gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+1, "B",j;
	  ampo+= omegap*gamma*t0*Cplx_i, "Cdag"+spin,j+1,"C"+spin,j, "B",j;
	  ampo += -omegap*gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+1, "Bdag",j;
	  ampo+= -omegap*gamma*t0*Cplx_i, "Cdag"+spin,j+1,"C"+spin,j, "Bdag",j ;
	}


	    ampo+= 2*t0*t0*t0*Cplx_i, "Cdag"+spin,1,"C"+spin,2;
	    ampo+= -2*t0*t0*t0*Cplx_i, "Cdag"+spin,2,"C"+spin,1;
	    ampo+= 2*t0*t0*t0*Cplx_i, "Cdag"+spin,N-1,"C"+spin,N;
	    ampo+= -2*t0*t0*t0*Cplx_i, "Cdag"+spin,N,"C"+spin,N-1;

}
   template<typename siteset>
   auto CurCorr_momentum2_left_withspin(siteset sites, double t0, double gamma, double omega, double omegap)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {

        auto ampo = AutoMPO(sites);
	get_CurCorr_momentum2_left_withspin( sites,ampo, "up",t0, gamma, omega, omegap);    	   
	get_CurCorr_momentum2_left_withspin( sites,ampo, "dn",t0, gamma, omega, omegap);    	   
    return ampo;
  }

   template<typename siteset>
   void gwt_CurCorr_momentum1_left_withspin(siteset sites, AutoMPO& ampo,std::string spin,double t0=1, double gamma=1)
   {
   int N=length(sites);
	    for(int j=1;j  < N; j+=1)
        {

	  ampo += gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+1, "B",j;
	  ampo+= gamma*t0*Cplx_i, "Cdag"+spin,j+1,"C"+spin,j, "B",j;
	  ampo += gamma*t0*Cplx_i,"Cdag"+spin,j,"C"+spin,j+1, "Bdag",j;
	  ampo+= gamma*t0*Cplx_i, "Cdag"+spin,j+1,"C"+spin,j, "Bdag",j ;
	}
    	    for(int j=2;j  <= N; j+=1)
        {

	  ampo += -gamma*t0*Cplx_i,"Cdag"+spin,j-1,"C"+spin,j, "B",j;
	  ampo+= -gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-1, "B",j ;
	  ampo += -gamma*t0*Cplx_i,"Cdag"+spin,j-1,"C"+spin,j, "Bdag",j;
	  ampo+= -gamma*t0*Cplx_i, "Cdag"+spin,j,"C"+spin,j-1, "Bdag",j ;
	}
	  	    	  ampo+= 2*t0*t0*Cplx_i, "Cdag"+spin,1,"C"+spin,1;
	    	  ampo+= -2*t0*t0*Cplx_i, "Cdag"+spin,N,"C"+spin,N;

}
   template<typename siteset>
  auto CurCorr_momentum1_left_withspin(siteset sites, double t0=1, double gamma=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
 
        auto ampo = AutoMPO(sites);
        gwt_CurCorr_momentum1_left_withspin( sites, ampo,"up",t0, gamma);
 gwt_CurCorr_momentum1_left_withspin( sites, ampo,"dn",t0, gamma);
    return ampo;
  }



     template<typename siteset>
    auto makeHolstHam_disp_withspin(siteset sites, double t0=1, double gamma=1, double omega=1, double omegap=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N; j+=1)
        {

      	ampo += -t0,"Cdagup",j,"Cup",j+1;
	ampo+= -t0, "Cdagup",j+1,"Cup",j ;
      	ampo += -t0,"Cdagdn",j,"Cdn",j+1;
	ampo+= -t0, "Cdagdn",j+1,"Cdn",j ;
	ampo += omegap,"Bdag",j,"B",j+1;
	ampo+= omegap, "Bdag",j+1,"B",j;
	}
    for(int j=1;j <= N; j += 1)
        {

	   ampo += gamma,"NtotX",j;
	    ampo += omega,"Nph",j;
        }

    return ampo;
  }
  // H_p -H_aux
    template<typename siteset>
  auto makeHolstHam_dispFTAux_withspin(siteset sites, double t0=1, double gamma=1, double omega=1, double omegap=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
    {
    int N=length(sites);

        auto ampo = AutoMPO(sites);
    	    for(int j=1;j  <= N-2; j+=2)
        {


      	ampo += -t0,"Cdagup",j,"Cup",j+2;
    	ampo+= -t0, "Cdagup",j+2,"Cup",j ;
      	ampo += -t0,"Cdagdn",j,"Cdn",j+2;
    	ampo+= -t0, "Cdagdn",j+2,"Cdn",j ;
	
      	ampo += omegap,"Bdag",j,"B",j+2;
    	ampo+= omegap, "Bdag",j+2,"B",j ;

    }
    for(int j=1;j < N; j += 2)
        {
    	   ampo += gamma,"NtotX",j;
	     ampo += omega,"Nph",j;
	 
        }
    	    for(int j=2;j  < N-1; j+=2)
        {


      	ampo += +t0,"Cdagup",j,"Cup",j+2;
    	ampo+= +t0, "Cdagup",j+2,"Cup",j ;
      	ampo += +t0,"Cdagdn",j,"Cdn",j+2;
    	ampo+= +t0, "Cdagdn",j+2,"Cdn",j ;
	
      	ampo += -omegap,"Bdag",j,"B",j+2;
    	ampo+= -omegap, "Bdag",j+2,"B",j ;

    }
     for(int j=2;j <= N; j += 2)
        {
    	   ampo += -gamma,"NtotX",j;
	     ampo += -omega,"Nph",j;
	 
        }
    
    return ampo;
  }
  // H_p with auxillery space
  template<typename siteset>
  auto makeHolstHam_dispFT_withspin(siteset sites, double t0=1, double gamma=1, double omega=1, double omegap=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
    {
    int N=length(sites);

        auto ampo = AutoMPO(sites);
    	    for(int j=1;j  <= N-2; j+=2)
        {


      	ampo += -t0,"Cdagup",j,"Cup",j+2;
    	ampo+= -t0, "Cdagup",j+2,"Cup",j ;
      	ampo += -t0,"Cdagdn",j,"Cdn",j+2;
    	ampo+= -t0, "Cdagdn",j+2,"Cdn",j ;
	
      	ampo += omegap,"Bdag",j,"B",j+2;
    	ampo+= omegap, "Bdag",j+2,"B",j ;

    }
    for(int j=1;j < N; j += 2)
        {
    	   ampo += gamma,"NtotX",j;
	     ampo += omega,"Nph",j;
	 
        }

   
    
    return ampo;
  }
class HolsteinSite_withspin_one;

using Holstein_withspin_one = BasicSiteSet<HolsteinSite_withspin_one>;
class HolsteinSite_withspin_one
    {
      Index s;


      
      std::vector<int> mvec;
      std::vector<Index> indexvec;
    public:

      HolsteinSite_withspin_one(Index I): s(I) { }

      //HolsteinSite(IQIndex I, int Mm=4) : M(Mm), s(I)  { } // not quite safe, how to control f the index contains right phonon number

      HolsteinSite_withspin_one( Args const& args = Args::global() )
        {
	  auto conserveQNs = true;
	  //args.getBool("ConserveQNs",true);
        // auto conserveNb = args.getBool("ConserveNb",conserveQNs);
        auto conserveNf = true;
	//args.getBool("ConserveNf",conserveQNs);
        auto conserveSz = true;
	//args.getBool("ConserveSz",conserveQNs);
	
	auto tags = TagSet("Site,Hol");
        auto n = 1;
        if(args.defined("SiteNumber") )
            {
            n = args.getInt("SiteNumber");
            tags.addTags("n="+str(n));
            }
	auto diffMaxOcc = args.getBool("DiffMaxOcc",false);
	auto maxOcc{0};
	if(diffMaxOcc)
	  {
	    auto occVec=args.getVecInt("MaxOccVec");
	    maxOcc= occVec[n-1];
	  }
	else{
        maxOcc = args.getInt("MaxOcc",1);
	}
	  //       if(not conserveQNs)
        //     {
	//       s = Index(4*(maxOcc+1),tags);
        //     }
        // else 
             {

	      auto q_occ_0 = QN({"Nf",0, -1},{"Sz",0});
	      auto q_occ_1 = QN({"Nf",1, -1},{"Sz",+1});
	      auto q_occ_2 = QN({"Nf",1, -1},{"Sz",-1});
	      auto q_occ_3 = QN({"Nf",2, -1},{"Sz",0});
	      //    if(not conserve_Nf) q_occ = QN({"Pf",1,-2});
            s = Index(q_occ_0,(maxOcc+1),
			q_occ_1,(maxOcc+1),q_occ_2,(maxOcc+1),q_occ_3,(maxOcc+1),Out,tags);
            }

	}
        // if(conserveQNs)
        //     {
        //     if(conserveNb)
        //         {
        //         auto qints = Index::qnstorage(1+maxOcc);
        //         for(int n : range(1+maxOcc)) 
        //             {
        //             qints[n] = QNInt(QN({"Nb",n}),1);
        //             }
        //         s = Index(std::move(qints),tags);
        //         }
        //     else
        //         {
        //         s = Index(QN(),1+maxOcc,tags);
        //         }
        //     }
        // else
        //     {
        //     if(conserveNb) Error("ConserveNb cannot be true when ConserveQNs=false");
        //     s = Index(1+maxOcc,tags);
        //     }
        // }


    Index
    index() const { return s; }

    IndexVal
    state(std::string const& state)
        {
	  auto d = static_cast<int>(dim(s)/4);
        if(state == "Emp" || state == "0") 
            {
            return s(1);
            }
        else 
        if(state == "Up" || state == "1") 
            {
            return s(d+1);
            }
        if(state == "Dn" || state == "2") 
            {
            return s(2*d+1);
            }
        if(state == "UpDn" || state == "2") 
            {
            return s(3*d+1);
            }
        else
            {
            Error("State " + state + " not recognized");
            }
        return IndexVal{};
        }

	ITensor
	op(std::string const& opname,
	   Args const& args) const
        {
        auto sP = prime(s);
	auto d = static_cast<int>(dim(s)/4);
	auto Op = ITensor(dag(s),sP);
	

        if(opname == "Ntot" || opname == "ntot")
            {

	      for (auto n :range1(d))
		{
		
		  Op.set(s=(d+n),sP=(d+n),1);
		  Op.set(s=(2*d+n),sP=(2*d+n),1);
		  Op.set(s=(3*d+n),sP=(3*d+n),2);
	      }
            }
	else        if(opname == "Nup" || opname == "nup")
            {

	      for (auto n :range1(d))
		{
		
		  Op.set(s=(d+n),sP=(d+n),1);

		  Op.set(s=(3*d+n),sP=(3*d+n),1);
	      }
            }
	else        if(opname == "Ndn" || opname == "ndn")
            {

	      for (auto n :range1(d))
		{
		
		  Op.set(s=(2*d+n),sP=(2*d+n),1);

		  Op.set(s=(3*d+n),sP=(3*d+n),1);
	      }
            }
	else
	  if(opname == "Nph")
	    {


	       
	      for(auto n :range1(d))
		{
	
		 Op.set(s=n, sP=n, n-1);
		 Op.set(s=(n+d), sP=(n+d), n-1);
		 Op.set(s=(n+2*d), sP=(n+2*d), n-1);
		 Op.set(s=(n+3*d), sP=(n+3*d), n-1);
 	      }
	    }
	  else
	     if(opname == "X" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,std::sqrt(n));
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=(2*d+n),sP=(2*d+1+n),std::sqrt(n));
		Op.set(s=(3*d+n),sP=(3*d+1+n),std::sqrt(n));

		Op.set(s=1+n,sP=n,std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
		Op.set(s=(2*d+n+1),sP=(2*d+n),std::sqrt(n));
		Op.set(s=(3*d+n+1),sP=(3*d+n),std::sqrt(n));
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "Bdag" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,std::sqrt(n));
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=(2*d+n),sP=(2*d+1+n),std::sqrt(n));
		Op.set(s=(3*d+n),sP=(3*d+1+n),std::sqrt(n));
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "B" )
            {

            for(auto n : range1(d-1))
                {
                Op.set(s=1+n,sP=n,std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
		Op.set(s=(2*d+n+1),sP=(2*d+n),std::sqrt(n));
		Op.set(s=(3*d+n+1),sP=(3*d+n),std::sqrt(n));
                }
	        if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
 	      
            }
	  else
	    	  if(opname == "NtotX" )
            {


            for(auto n : range1(d-1))
                {
		  
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=(2*d+n),sP=(2*d+1+n),std::sqrt(n));
		Op.set(s=(3*d+n),sP=(3*d+1+n),2*std::sqrt(n));

		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
		Op.set(s=(2*d+n+1),sP=(2*d+n),std::sqrt(n));
		Op.set(s=(3*d+n+1),sP=(3*d+n),2*std::sqrt(n));
		
                }
	     if(d-1==0)
	       {

		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
		  if(opname == "NtotBdag" )
            {


            for(auto n : range1(d-1))
                {
		  
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=(2*d+n),sP=(2*d+1+n),std::sqrt(n));
		Op.set(s=(3*d+n),sP=(3*d+1+n),2*std::sqrt(n));
                }
	     if(d-1==0)
	       {

		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "NtotB" )
            {

            for(auto n : range1(d-1))
                {
                
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
		Op.set(s=(2*d+n+1),sP=(2*d+n),std::sqrt(n));
		Op.set(s=(3*d+n+1),sP=(3*d+n),2*std::sqrt(n));
                }
	     if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
 	      
            }
        else
        if(opname == "Cup")
            {
	      
 
		for(auto n : range1(d))
		  {
 
		 Op.set(s=(d+n), sP=(n), 1);
		 Op.set(s=(3*d+n), sP=(2*d+n), 1);
	      }

            }
        else
        if(opname == "Cdagup")
            {

		for(auto n : range1(d))
                {

		Op.set( s=n, sP=(d+n), 1);
		Op.set( s=2*d+n, sP=(3*d+n), 1);
	      }

            }
        else
        if(opname == "Cdn")
            {
	      
 
		for(auto n : range1(d))
		  {
 
		 Op.set(s=(2*d+n), sP=(n), 1);
		 Op.set(s=(3*d+n), sP=(1*d+n), -1);
	      }

            }
        else
        if(opname == "Cdagdn")
            {

		for(auto n : range1(d))
                {

		Op.set( s=n, sP=(2*d+n), 1);
		Op.set( s=1*d+n, sP=(3*d+n), -1);
	      }

            }
        else
        if(opname == "Aup")
	  {

		for(auto n : range1(d))
                {
	
		 Op.set(s=(d+n), sP=(n), 1);
		 Op.set(s=(3*d+n), sP=(2*d+n), 1);
	      }

	    
            }
        else
        if(opname == "Adagup")
            {

		for(auto n : range1(d))
                {
	
		Op.set( s=n, sP=(d+n), 1);
		Op.set( s=2*d+n, sP=(3*d+n), 1);
	      }	      

            }
	else
        if(opname == "Adn")
	  {

		for(auto n : range1(d))
                {
	
		 Op.set(s=(2*d+n), sP=(n), 1);
		 Op.set(s=(3*d+n), sP=(1*d+n), 1);
	      }

	    
            }
	else
        if(opname == "Adagdn")
            {

		for(auto n : range1(d))
                {
	
		Op.set( s=n, sP=(2*d+n), 1);
		Op.set( s=1*d+n, sP=(3*d+n), 1);
	      }	      

            }
        else
        if(opname == "F" || opname == "FermiPhase")
            {

	      		for(auto n : range1(d))
                {

            Op.set(s=n,sP=n,+1);
            Op.set(s=(1*d+n), sP=(d+n),-1);
	    Op.set(s=(2*d+n), sP=(2*d+n),-1);
	    Op.set(s=(3*d+n), sP=(3*d+n),+1);
	      }

            }
        else
        if(opname == "Fup" )
            {

	      		for(auto n : range1(d))
                {

            Op.set(s=n,sP=n,+1);
            Op.set(s=(1*d+n), sP=(d+n),-1);
	    Op.set(s=(2*d+n), sP=(2*d+n),+1);
	    Op.set(s=(3*d+n), sP=(3*d+n),-1);
	      }

            }
        else
        if(opname == "Fdn" )
            {

	      		for(auto n : range1(d))
                {

            Op.set(s=n,sP=n,+1);
            Op.set(s=(1*d+n), sP=(d+n),+1);
	    Op.set(s=(2*d+n), sP=(2*d+n),-1);
	    Op.set(s=(3*d+n), sP=(3*d+n),-1);
	      }

            }
	else
        if(opname == "Sz")
            {
 		for(auto n : range1(d))
                {


            Op.set(s=(1*d+n), sP=(d+n),+0.5);
	    Op.set(s=(2*d+n), sP=(2*d+n),-0.5);

	      }
            }
        else
            {
            Error("Operator \"" + opname + "\" name not recognized");
            }

        return Op;
        }

      HolsteinSite_withspin_one(int n, Args const& args = Args::global())
      {
        *this = HolsteinSite_withspin_one({args,"SiteNumber=",n});
      }
};

class HolsteinSite_withspin_two;

using Holstein_withspin_two = BasicSiteSet<HolsteinSite_withspin_two>;
class HolsteinSite_withspin_two
    {
      Index s;


      
      std::vector<int> mvec;
      std::vector<Index> indexvec;
    public:

      HolsteinSite_withspin_two(Index I): s(I) { }

      //HolsteinSite(IQIndex I, int Mm=4) : M(Mm), s(I)  { } // not quite safe, how to control f the index contains right phonon number

      HolsteinSite_withspin_two( Args const& args = Args::global() )
        {
	  auto conserveQNs = true;
	  //args.getBool("ConserveQNs",true);
        // auto conserveNb = args.getBool("ConserveNb",conserveQNs);
        auto conserveNf = true;
	//args.getBool("ConserveNf",conserveQNs);
        auto conserveSz = true;
	//args.getBool("ConserveSz",conserveQNs);
	
	auto tags = TagSet("Site,Hol");
        auto n = 1;
        if(args.defined("SiteNumber") )
            {
            n = args.getInt("SiteNumber");
            tags.addTags("n="+str(n));
            }
	auto diffMaxOcc = args.getBool("DiffMaxOcc",false);
	auto maxOcc{0};
	if(diffMaxOcc)
	  {
	    auto occVec=args.getVecInt("MaxOccVec");
	    maxOcc= occVec[n-1];
	  }
	else{
        maxOcc = args.getInt("MaxOcc",1);
	}
	  //       if(not conserveQNs)
        //     {
	//       s = Index(4*(maxOcc+1),tags);
        //     }
        // else 
             {

	      auto q_occ_0 = QN({"Nf_2",0, -1},{"Sz_2",0});
	      auto q_occ_1 = QN({"Nf_2",1, -1},{"Sz_2",+1});
	      auto q_occ_2 = QN({"Nf_2",1, -1},{"Sz_2",-1});
	      auto q_occ_3 = QN({"Nf_2",2, -1},{"Sz_2",0});
	      //    if(not conserve_Nf) q_occ = QN({"Pf",1,-2});
            s = Index(q_occ_0,(maxOcc+1),
			q_occ_1,(maxOcc+1),q_occ_2,(maxOcc+1),q_occ_3,(maxOcc+1),Out,tags);
            }

	}
        // if(conserveQNs)
        //     {
        //     if(conserveNb)
        //         {
        //         auto qints = Index::qnstorage(1+maxOcc);
        //         for(int n : range(1+maxOcc)) 
        //             {
        //             qints[n] = QNInt(QN({"Nb",n}),1);
        //             }
        //         s = Index(std::move(qints),tags);
        //         }
        //     else
        //         {
        //         s = Index(QN(),1+maxOcc,tags);
        //         }
        //     }
        // else
        //     {
        //     if(conserveNb) Error("ConserveNb cannot be true when ConserveQNs=false");
        //     s = Index(1+maxOcc,tags);
        //     }
        // }


    Index
    index() const { return s; }

    IndexVal
    state(std::string const& state)
        {
	  auto d = static_cast<int>(dim(s)/4);
        if(state == "Emp" || state == "0") 
            {
            return s(1);
            }
        else 
        if(state == "Up" || state == "1") 
            {
            return s(d+1);
            }
        if(state == "Dn" || state == "2") 
            {
            return s(2*d+1);
            }
        if(state == "UpDn" || state == "2") 
            {
            return s(3*d+1);
            }
        else
            {
            Error("State " + state + " not recognized");
            }
        return IndexVal{};
        }

	ITensor
	op(std::string const& opname,
	   Args const& args) const
        {
        auto sP = prime(s);
	auto d = static_cast<int>(dim(s)/4);
	auto Op = ITensor(dag(s),sP);
	

        if(opname == "Ntot" || opname == "ntot")
            {

	      for (auto n :range1(d))
		{
		
		  Op.set(s=(d+n),sP=(d+n),1);
		  Op.set(s=(2*d+n),sP=(2*d+n),1);
		  Op.set(s=(3*d+n),sP=(3*d+n),2);
	      }
            }
	else        if(opname == "Nup" || opname == "nup")
            {

	      for (auto n :range1(d))
		{
		
		  Op.set(s=(d+n),sP=(d+n),1);

		  Op.set(s=(3*d+n),sP=(3*d+n),1);
	      }
            }
	else        if(opname == "Ndn" || opname == "ndn")
            {

	      for (auto n :range1(d))
		{
		
		  Op.set(s=(2*d+n),sP=(2*d+n),1);

		  Op.set(s=(3*d+n),sP=(3*d+n),1);
	      }
            }
	else
	  if(opname == "Nph")
	    {


	       
	      for(auto n :range1(d))
		{
	
		 Op.set(s=n, sP=n, n-1);
		 Op.set(s=(n+d), sP=(n+d), n-1);
		 Op.set(s=(n+2*d), sP=(n+2*d), n-1);
		 Op.set(s=(n+3*d), sP=(n+3*d), n-1);
 	      }
	    }
	  else
	     if(opname == "X" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,std::sqrt(n));
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=(2*d+n),sP=(2*d+1+n),std::sqrt(n));
		Op.set(s=(3*d+n),sP=(3*d+1+n),std::sqrt(n));

		Op.set(s=1+n,sP=n,std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
		Op.set(s=(2*d+n+1),sP=(2*d+n),std::sqrt(n));
		Op.set(s=(3*d+n+1),sP=(3*d+n),std::sqrt(n));
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "Bdag" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,std::sqrt(n));
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=(2*d+n),sP=(2*d+1+n),std::sqrt(n));
		Op.set(s=(3*d+n),sP=(3*d+1+n),std::sqrt(n));
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "B" )
            {

            for(auto n : range1(d-1))
                {
                Op.set(s=1+n,sP=n,std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
		Op.set(s=(2*d+n+1),sP=(2*d+n),std::sqrt(n));
		Op.set(s=(3*d+n+1),sP=(3*d+n),std::sqrt(n));
                }
	        if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
 	      
            }
	  else
	    	  if(opname == "NtotX" )
            {


            for(auto n : range1(d-1))
                {
		  
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=(2*d+n),sP=(2*d+1+n),std::sqrt(n));
		Op.set(s=(3*d+n),sP=(3*d+1+n),2*std::sqrt(n));

		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
		Op.set(s=(2*d+n+1),sP=(2*d+n),std::sqrt(n));
		Op.set(s=(3*d+n+1),sP=(3*d+n),2*std::sqrt(n));
		
                }
	     if(d-1==0)
	       {

		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
		  if(opname == "NtotBdag" )
            {


            for(auto n : range1(d-1))
                {
		  
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=(2*d+n),sP=(2*d+1+n),std::sqrt(n));
		Op.set(s=(3*d+n),sP=(3*d+1+n),2*std::sqrt(n));
                }
	     if(d-1==0)
	       {

		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "NtotB" )
            {

            for(auto n : range1(d-1))
                {
                
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
		Op.set(s=(2*d+n+1),sP=(2*d+n),std::sqrt(n));
		Op.set(s=(3*d+n+1),sP=(3*d+n),2*std::sqrt(n));
                }
	     if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
 	      
            }
        else
        if(opname == "Cup")
            {
	      
 
		for(auto n : range1(d))
		  {
 
		 Op.set(s=(d+n), sP=(n), 1);
		 Op.set(s=(3*d+n), sP=(2*d+n), 1);
	      }

            }
        else
        if(opname == "Cdagup")
            {

		for(auto n : range1(d))
                {

		Op.set( s=n, sP=(d+n), 1);
		Op.set( s=2*d+n, sP=(3*d+n), 1);
	      }

            }
        else
        if(opname == "Cdn")
            {
	      
 
		for(auto n : range1(d))
		  {
 
		 Op.set(s=(2*d+n), sP=(n), 1);
		 Op.set(s=(3*d+n), sP=(1*d+n), -1);
	      }

            }
        else
        if(opname == "Cdagdn")
            {

		for(auto n : range1(d))
                {

		Op.set( s=n, sP=(2*d+n), 1);
		Op.set( s=1*d+n, sP=(3*d+n), -1);
	      }

            }
        else
        if(opname == "Aup")
	  {

		for(auto n : range1(d))
                {
	
		 Op.set(s=(d+n), sP=(n), 1);
		 Op.set(s=(3*d+n), sP=(2*d+n), 1);
	      }

	    
            }
        else
        if(opname == "Adagup")
            {

		for(auto n : range1(d))
                {
	
		Op.set( s=n, sP=(d+n), 1);
		Op.set( s=2*d+n, sP=(3*d+n), 1);
	      }	      

            }
	else
        if(opname == "Adn")
	  {

		for(auto n : range1(d))
                {
	
		 Op.set(s=(2*d+n), sP=(n), 1);
		 Op.set(s=(3*d+n), sP=(1*d+n), 1);
	      }

	    
            }
	else
        if(opname == "Adagdn")
            {

		for(auto n : range1(d))
                {
	
		Op.set( s=n, sP=(2*d+n), 1);
		Op.set( s=1*d+n, sP=(3*d+n), 1);
	      }	      

            }
        else
        if(opname == "F" || opname == "FermiPhase")
            {

	      		for(auto n : range1(d))
                {

            Op.set(s=n,sP=n,+1);
            Op.set(s=(1*d+n), sP=(d+n),-1);
	    Op.set(s=(2*d+n), sP=(2*d+n),-1);
	    Op.set(s=(3*d+n), sP=(3*d+n),+1);
	      }

            }
        else
        if(opname == "Fup" )
            {

	      		for(auto n : range1(d))
                {

            Op.set(s=n,sP=n,+1);
            Op.set(s=(1*d+n), sP=(d+n),-1);
	    Op.set(s=(2*d+n), sP=(2*d+n),+1);
	    Op.set(s=(3*d+n), sP=(3*d+n),-1);
	      }

            }
        else
        if(opname == "Fdn" )
            {

	      		for(auto n : range1(d))
                {

            Op.set(s=n,sP=n,+1);
            Op.set(s=(1*d+n), sP=(d+n),+1);
	    Op.set(s=(2*d+n), sP=(2*d+n),-1);
	    Op.set(s=(3*d+n), sP=(3*d+n),-1);
	      }

            }
	else
        if(opname == "Sz")
            {
 		for(auto n : range1(d))
                {


            Op.set(s=(1*d+n), sP=(d+n),+0.5);
	    Op.set(s=(2*d+n), sP=(2*d+n),-0.5);

	      }
            }
        else
            {
            Error("Operator \"" + opname + "\" name not recognized");
            }

        return Op;
        }

      HolsteinSite_withspin_two(int n, Args const& args = Args::global())
      {
        *this = HolsteinSite_withspin_two({args,"SiteNumber=",n});
      }
};




    template<typename T>
  struct is_Mixed_hol_withspin: std::false_type {
  };
 using  Holstein_exp_withspin = MixedSiteSet<HolsteinSite_withspin_one,HolsteinSite_withspin_two>;
  template<>
  struct is_Mixed_hol_withspin<Holstein_exp_withspin>: std::true_type {
   
  };

} //namespace itensor
#endif /* HOLSTEIN_TWO_SPIN_H */
