
//
// Distributed under the ITensor Library License, Version 1.2
//    (See accompanying LICENSE file.)
//
 #ifndef HOLSTEIN_HAM2D_H
#define HOLSTEIN_HAM2D_H
#include "itensor/mps/siteset.h"
#include "itensor/all.h"
#include"extra.hpp"
namespace itensor {

  // 2dimensional systems
  /* MPO in snake formation
1 2 3 4 
8  7  6  5
9 10 11 12
16 15   14   13
  */
    template<typename siteset>
     auto makeHolstHam_2d(siteset sites, int L,double t0=1, double gamma=1, double omega=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N; j+=1)
        {

      	ampo += -t0,"Cdag",j,"C",j+1;
	ampo+= -t0, "Cdag",j+1,"C",j ;

	}
	 //    
	    // iterating bonds 1, 9 etc, leaving the "right "edges out
	    for(int i=1;i  <L*L-L; i+=2*L)
          {
	     for(int j=i;j  <i+L-1; j+=1)
          {
	    int ind_1=j;
	    int ind_2=i+2*L-(j-i+1);
      	  ampo += -t0,"Cdag",ind_1,"C",ind_2;
	  ampo+= -t0, "Cdag",ind_2,"C",ind_1 ;
	  	    std::cout<< "connecting sites  "<<ind_1 << " and "<< ind_2<<std::endl; 
	  }
	  }
std::cout <<std::endl;	  
  // iterating edges 8 etc
	    for(int i=2*L;i  <=L*L-L; i+=2*L)
          {
	    
	     for(int j=i;j  >i-L+1; j-=1)
          {
	    int ind_1=j;
	    int ind_2=i-(j-i-1);
	    
	       ampo += -t0,"Cdag",ind_1,"C",ind_2;
	      ampo+= -t0, "Cdag",ind_2,"C",ind_1 ;
	    // std::cout<< " site "<< i<<std::endl;
	    std::cout<< "connecting sites  "<<ind_1 << " and "<< ind_2<<std::endl; 
	  }
	  }
    for(int j=1;j <= N; j += 1)
        {

	   ampo += gamma,"NX",j;
	    ampo += omega,"Nph",j;
        }

    return ampo;
  }









} //namespace itensor
#endif /* HOLSTEIN_HAM2D_H */
