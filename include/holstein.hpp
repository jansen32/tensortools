
//
// Distributed under the ITensor Library License, Version 1.2
//    (See accompanying LICENSE file.)
//
 #ifndef HOLSTEIN_H
#define HOLSTEIN_H
#include "itensor/mps/siteset.h"
#include "itensor/all.h"
#include"extra.hpp"
namespace itensor {

  template<typename siteset>
auto CDWOparam(siteset sites)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	int i=-1;
	    for(int j=1;j  <= N; j+=1)
        {

      	ampo += i,"N",j;

	i*=-1;
	}
    

    return ampo;
  }
  template<typename siteset>
auto CDWOparam_FT(siteset sites)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	int i=-1;
	    for(int j=1;j  < N; j+=2)
        {

      	ampo += i,"N",j;

	i*=-1;
	}
    

    return ampo;
  }

  template<typename siteset>
auto CDWDispOparam(siteset sites)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	int i=-1;
	    for(int j=1;j  <= N; j+=1)
        {

      	ampo += i,"B",j;
	ampo += i,"Bdag",j;
	i*=-1;
	}
    

    return ampo;
  }
  template<typename siteset>
auto CDWDispOparam_FT(siteset sites)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	int i=-1;
	    for(int j=1;j  < N; j+=2)
        {

      	ampo += i,"B",j;
	ampo += i,"Bdag",j;
	i*=-1;
	}
    

    return ampo;
  }


  template<typename siteset>
  auto CurCorr_momentum1_left_FT(siteset sites, double t0=1, double gamma=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
    //auto pref=
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  <= N-3; j+=2)
        {

	  ampo += gamma*t0*Cplx_i,"Cdag",j,"C",j+2, "B",j;
	  ampo+= gamma*t0*Cplx_i, "Cdag",j+2,"C",j, "B",j ;
	  ampo += gamma*t0*Cplx_i,"Cdag",j,"C",j+2, "Bdag",j;
	  ampo+= gamma*t0*Cplx_i, "Cdag",j+2,"C",j, "Bdag",j ;
	}
    	    for(int j=3;j  <= N-1; j+=2)
        {

	  ampo += -gamma*t0*Cplx_i,"Cdag",j-2,"C",j, "B",j;
	  ampo+= -gamma*t0*Cplx_i, "Cdag",j,"C",j-2, "B",j ;
	  ampo +=-gamma*t0*Cplx_i,"Cdag",j-2,"C",j, "Bdag",j;
	  ampo+= -gamma*t0*Cplx_i, "Cdag",j,"C",j-2, "Bdag",j ;
	}
	  
	     ampo+= 2*t0*t0*Cplx_i, "Cdag",1,"C",1;
	    	  ampo+= -2*t0*t0*Cplx_i, "Cdag",N-1,"C",N-1;
    return ampo;
  }

   template<typename siteset>
   auto CurCorr_momentum2_left_FT(siteset sites, double t0, double gamma, double omega, double omegap)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
    	    for(int j=3;j  <= N-1; j+=2)
        {

	  ampo += omega*gamma*t0*Cplx_i,"Cdag",j-2,"C",j, "B",j;
	  ampo+= omega*gamma*t0*Cplx_i, "Cdag",j,"C",j-2, "B",j ;
	  ampo += -omega*gamma*t0*Cplx_i,"Cdag",j-2,"C",j, "Bdag",j;
	  ampo+= -omega*gamma*t0*Cplx_i, "Cdag",j,"C",j-2, "Bdag",j ;
	}
   	    for(int j=1;j  < N-1; j+=2)
        {

   	  ampo += -omega*gamma*t0*Cplx_i,"Cdag",j,"C",j+2, "B",j;
   	  ampo+= -omega*gamma*t0*Cplx_i, "Cdag",j+2,"C",j, "B",j;
   	  ampo += omega*gamma*t0*Cplx_i,"Cdag",j,"C",j+2, "Bdag",j;
   	  ampo+= omega*gamma*t0*Cplx_i, "Cdag",j+2,"C",j, "Bdag",j ;
   	}
    	 
   	    for(int j=1;j  < N-1; j+=2)
        {

   	  ampo += -gamma*gamma*t0*Cplx_i,"Cdag",j,"C",j+2, "X",j, "X",j+2;
   	  ampo+= -gamma*gamma*t0*Cplx_i, "Cdag",j+2,"C",j, "X",j, "X",j;
   	  ampo += gamma*gamma*t0*Cplx_i,"Cdag",j,"C",j+2, "X",j, "X",j;
   	  ampo+= gamma*gamma*t0*Cplx_i, "Cdag",j+2,"C",j, "X",j, "X",j+2 ;
   	}
   for(int j=3;j  <= N-1; j+=2)
        {

   	  ampo += gamma*gamma*t0*Cplx_i,"Cdag",j-2,"C",j, "X",j, "X",j;
   	  ampo+= gamma*gamma*t0*Cplx_i, "Cdag",j,"C",j-2, "X",j, "X",j-2;
   	  ampo += -gamma*gamma*t0*Cplx_i,"Cdag",j-2,"C",j, "X",j, "X",j-2;
   	  ampo+= -gamma*gamma*t0*Cplx_i, "Cdag",j,"C",j-2, "X",j, "X",j ;
   	}

   // double check this sign
	    for(int j=1;j  < N-3; j+=2)
        {

   	  ampo += -gamma*t0*t0*Cplx_i,"Cdag",j+4,"C",j, "X",j;
   	  ampo+= gamma*t0*t0*Cplx_i, "Cdag",j,"C",j+4, "X",j;
   	  ampo += -gamma*t0*t0*Cplx_i,"Cdag",j,"C",j+4, "X",j+2;
   	  ampo+= gamma*t0*t0*Cplx_i, "Cdag",j+4,"C",j, "X",j+2 ;
   	}
   for(int j=3;j  < N-1; j+=2)
        {

   	  ampo += -gamma*t0*t0*Cplx_i,"Cdag",j-2,"C",j+2, "X",j;
   	  ampo+= gamma*t0*t0*Cplx_i, "Cdag",j+2,"C",j-2, "X",j;
   	  ampo += -gamma*t0*t0*Cplx_i,"Cdag",j+2,"C",j-2, "X",j+2;
   	  ampo+= gamma*t0*t0*Cplx_i, "Cdag",j-2,"C",j+2, "X",j+2 ;
   	}
    	    for(int j=3;j  < N-1; j+=1)
        {

   	  ampo += omegap*gamma*t0*Cplx_i,"Cdag",j-2,"C",j, "B",j+2;
   	  ampo+= omegap*gamma*t0*Cplx_i, "Cdag",j,"C",j-2, "B",j+2 ;
   	  ampo += -omegap*gamma*t0*Cplx_i,"Cdag",j-2,"C",j, "Bdag",j+2;
   	  ampo+= -omegap*gamma*t0*Cplx_i, "Cdag",j,"C",j-2, "Bdag",j+2 ;

   	  ampo += omegap*gamma*t0*Cplx_i,"Cdag",j,"C",j+2, "B",j;
   	  ampo+= omegap*gamma*t0*Cplx_i, "Cdag",j+2,"C",j, "B",j;
   	  ampo += -omegap*gamma*t0*Cplx_i,"Cdag",j,"C",j+2, "Bdag",j;
   	  ampo+= -omegap*gamma*t0*Cplx_i, "Cdag",j+2,"C",j, "Bdag",j ;
   	}


   	    ampo+= 2*t0*t0*t0*Cplx_i, "Cdag",1,"C",3;
   	    ampo+= -2*t0*t0*t0*Cplx_i, "Cdag",3,"C",1;
   	    ampo+= 2*t0*t0*t0*Cplx_i, "Cdag",N-3,"C",N-1;
   	    ampo+= -2*t0*t0*t0*Cplx_i, "Cdag",N-1,"C",N-3;

    return ampo;
  }


   template<typename siteset>
   auto CurCorr_momentum2_left(siteset sites, double t0, double gamma, double omega, double omegap)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
    	    for(int j=2;j  <= N; j+=1)
        {

	  ampo += omega*gamma*t0*Cplx_i,"Cdag",j-1,"C",j, "B",j;
	  ampo+= omega*gamma*t0*Cplx_i, "Cdag",j,"C",j-1, "B",j ;
	  ampo += -omega*gamma*t0*Cplx_i,"Cdag",j-1,"C",j, "Bdag",j;
	  ampo+= -omega*gamma*t0*Cplx_i, "Cdag",j,"C",j-1, "Bdag",j ;
	}
	    for(int j=1;j  < N; j+=1)
        {

	  ampo += -omega*gamma*t0*Cplx_i,"Cdag",j,"C",j+1, "B",j;
	  ampo+= -omega*gamma*t0*Cplx_i, "Cdag",j+1,"C",j, "B",j;
	  ampo += omega*gamma*t0*Cplx_i,"Cdag",j,"C",j+1, "Bdag",j;
	  ampo+= omega*gamma*t0*Cplx_i, "Cdag",j+1,"C",j, "Bdag",j ;
	}
    	 
	    for(int j=1;j  < N; j+=1)
        {

	  ampo += -gamma*gamma*t0*Cplx_i,"Cdag",j,"C",j+1, "X",j, "X",j+1;
	  ampo+= -gamma*gamma*t0*Cplx_i, "Cdag",j+1,"C",j, "X",j, "X",j;
	  ampo += gamma*gamma*t0*Cplx_i,"Cdag",j,"C",j+1, "X",j, "X",j;
	  ampo+= gamma*gamma*t0*Cplx_i, "Cdag",j+1,"C",j, "X",j, "X",j+1 ;
	}
   for(int j=2;j  <= N; j+=1)
        {

	  ampo += gamma*gamma*t0*Cplx_i,"Cdag",j-1,"C",j, "X",j, "X",j;
	  ampo+= gamma*gamma*t0*Cplx_i, "Cdag",j,"C",j-1, "X",j, "X",j-1;
	  ampo += -gamma*gamma*t0*Cplx_i,"Cdag",j-1,"C",j, "X",j, "X",j-1;
	  ampo+= -gamma*gamma*t0*Cplx_i, "Cdag",j,"C",j-1, "X",j, "X",j ;
	}

   // double check this sign
	    for(int j=1;j  < N-1; j+=1)
        {

	  ampo += -gamma*t0*t0*Cplx_i,"Cdag",j+2,"C",j, "X",j;
	  ampo+= gamma*t0*t0*Cplx_i, "Cdag",j,"C",j+2, "X",j;
	  ampo += -gamma*t0*t0*Cplx_i,"Cdag",j,"C",j+2, "X",j+1;
	  ampo+= gamma*t0*t0*Cplx_i, "Cdag",j+2,"C",j, "X",j+1 ;
	}
   for(int j=2;j  < N; j+=1)
        {

	  ampo += -gamma*t0*t0*Cplx_i,"Cdag",j-1,"C",j+1, "X",j;
	  ampo+= gamma*t0*t0*Cplx_i, "Cdag",j+1,"C",j-1, "X",j;
	  ampo += -gamma*t0*t0*Cplx_i,"Cdag",j+1,"C",j-1, "X",j+1;
	  ampo+= gamma*t0*t0*Cplx_i, "Cdag",j-1,"C",j+1, "X",j+1 ;
	}
    	    for(int j=2;j  < N; j+=1)
        {

	  ampo += omegap*gamma*t0*Cplx_i,"Cdag",j-1,"C",j, "B",j+1;
	  ampo+= omegap*gamma*t0*Cplx_i, "Cdag",j,"C",j-1, "B",j+1 ;
	  ampo += -omegap*gamma*t0*Cplx_i,"Cdag",j-1,"C",j, "Bdag",j+1;
	  ampo+= -omegap*gamma*t0*Cplx_i, "Cdag",j,"C",j-1, "Bdag",j+1 ;

	  ampo += omegap*gamma*t0*Cplx_i,"Cdag",j,"C",j+1, "B",j;
	  ampo+= omegap*gamma*t0*Cplx_i, "Cdag",j+1,"C",j, "B",j;
	  ampo += -omegap*gamma*t0*Cplx_i,"Cdag",j,"C",j+1, "Bdag",j;
	  ampo+= -omegap*gamma*t0*Cplx_i, "Cdag",j+1,"C",j, "Bdag",j ;
	}


	    ampo+= 2*t0*t0*t0*Cplx_i, "Cdag",1,"C",2;
	    ampo+= -2*t0*t0*t0*Cplx_i, "Cdag",2,"C",1;
	    ampo+= 2*t0*t0*t0*Cplx_i, "Cdag",N-1,"C",N;
	    ampo+= -2*t0*t0*t0*Cplx_i, "Cdag",N,"C",N-1;

    return ampo;
  }


   template<typename siteset>
  auto CurCorr_momentum1_left(siteset sites, double t0=1, double gamma=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N; j+=1)
        {

	  ampo += gamma*t0*Cplx_i,"Cdag",j,"C",j+1, "B",j;
	  ampo+= gamma*t0*Cplx_i, "Cdag",j+1,"C",j, "B",j;
	  ampo += gamma*t0*Cplx_i,"Cdag",j,"C",j+1, "Bdag",j;
	  ampo+= gamma*t0*Cplx_i, "Cdag",j+1,"C",j, "Bdag",j ;
	}
    	    for(int j=2;j  <= N; j+=1)
        {

	  ampo += -gamma*t0*Cplx_i,"Cdag",j-1,"C",j, "B",j;
	  ampo+= -gamma*t0*Cplx_i, "Cdag",j,"C",j-1, "B",j ;
	  ampo += -gamma*t0*Cplx_i,"Cdag",j-1,"C",j, "Bdag",j;
	  ampo+= -gamma*t0*Cplx_i, "Cdag",j,"C",j-1, "Bdag",j ;
	}
	  	    	  ampo+= 2*t0*t0*Cplx_i, "Cdag",1,"C",1;
	    	  ampo+= -2*t0*t0*Cplx_i, "Cdag",N,"C",N;

    return ampo;
  }

  
  template<typename siteset>
auto makeCurr(siteset sites, double t0=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N; j+=1)
        {

      	ampo += t0*Cplx_i,"Cdag",j,"C",j+1;
	ampo+= -t0*Cplx_i, "Cdag",j+1,"C",j ;
	}
    

    return ampo;
  }



      template<typename siteset>
      auto makeECurr(siteset sites, double t0, double gamma, double omega, double omegap)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=2;j  < N; j+=1)
        {

      	ampo += t0*t0*Cplx_i,"Cdag",j-1,"C",j+1;
	ampo+= -t0*t0*Cplx_i, "Cdag",j+1,"C",j-1;
	
	ampo += -t0*gamma*Cplx_i,"Bdag",j,"Cdag",j-1,"C",j;
	ampo+= -t0*gamma*Cplx_i,"B",j, "Cdag",j-1,"C",j ;
	ampo += t0*gamma*Cplx_i,"Bdag",j,"Cdag",j,"C",j-1;
	ampo+= t0*gamma*Cplx_i,"B",j, "Cdag",j,"C",j-1 ;
	ampo+= omegap*gamma*Cplx_i,"n",j, "Bdag",j-1;
	ampo+= -omegap*gamma*Cplx_i,"n",j, "B",j-1;
	ampo+= omegap*omega*Cplx_i,"Bdag",j-1, "B",j;
	ampo+= -omegap*omega*Cplx_i,"Bdag",j, "B",j-1;
	ampo+= omegap*omegap*Cplx_i,"Bdag",j-1, "B",j+1;
	ampo+= -omegap*omega*Cplx_i,"Bdag",j+1, "B",j-1;
	
	}
    

    return ampo;
  }
  
    template<typename siteset>
    auto makeCurr_probe(siteset sites, double t0=1, std::complex<double> factor=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N; j+=1)
        {

      	ampo += t0*factor*Cplx_i,"Cdag",j,"C",j+1;
	ampo+= -t0*std::conj(factor)*Cplx_i, "Cdag",j+1,"C",j ;
	}
    

    return ampo;
  }
    template<typename siteset>
auto makeCurr_FT(siteset sites, double t0=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N-2; j+=2)
        {

	  ampo += t0*Cplx_i,"Cdag",j,"C",j+2;
	   	ampo+= -t0*Cplx_i, "Cdag",j+2,"C",j ;
	  // ampo+= 1, "n",j, "n", j+2;
	}
    

    return ampo;
  }
    template<typename siteset>
      auto makeECurr_dispFT(siteset sites, double t0, double gamma, double omega, double omegap)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
    for(int j=3;j  <= N-3; j+=2)
        {

      	ampo += t0*t0*Cplx_i,"Cdag",j-2,"C",j+2;
	ampo+= -t0*t0*Cplx_i, "Cdag",j+2,"C",j-2;

  	ampo += omegap*omegap*Cplx_i,"Bdag",j-2,"B",j+2;
	ampo+= -omegap*omegap*Cplx_i, "Bdag",j+2,"B",j-2;
	}
	    for(int j=3; j<= N-1; j+=2)
        {
	ampo += -t0*gamma*Cplx_i,"Bdag",j,"Cdag",j-2,"C",j;
	ampo+= -t0*gamma*Cplx_i,"B",j, "Cdag",j-2,"C",j ;
	ampo += t0*gamma*Cplx_i,"Bdag",j,"Cdag",j,"C",j-2;
	ampo+= t0*gamma*Cplx_i,"B",j, "Cdag",j,"C",j-2 ;

	ampo+= omegap*gamma*Cplx_i,"n",j, "Bdag",j-2;
	ampo+= -omegap*gamma*Cplx_i,"n",j, "B",j-2;

	ampo+= omegap*omega*Cplx_i,"Bdag",j-2, "B",j;
	ampo+= -omegap*omega*Cplx_i,"Bdag",j, "B",j-2;
	
}
    	
    

    return ampo;
  }


    template<typename siteset>
      auto makeECurr_dispPHFT(siteset sites, double t0, double gamma, double omega, double omegap)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
    for(int j=3;j  <= N-3; j+=2)
        {

  	ampo += omegap*omegap*Cplx_i,"Bdag",j-2,"B",j+2;
	ampo+= -omegap*omegap*Cplx_i, "Bdag",j+2,"B",j-2;
	}
	    for(int j=3; j<= N-1; j+=2)
        {

	ampo+= omegap*gamma*Cplx_i,"n",j, "Bdag",j-2;
	ampo+= -omegap*gamma*Cplx_i,"n",j, "B",j-2;

	ampo+= omegap*omega*Cplx_i,"Bdag",j-2, "B",j;
	ampo+= -omegap*omega*Cplx_i,"Bdag",j, "B",j-2;
	
}
    	
    

    return ampo;
  }
 template<typename siteset>
      auto makeECurr_RED_FT(siteset sites, double t0, double gamma, double omega)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	
	    for(int j=3; j<= N-1; j+=2)
        {
	ampo += -t0*gamma*Cplx_i,"Bdag",j,"Cdag",j-2,"C",j;
	ampo+= -t0*gamma*Cplx_i,"B",j, "Cdag",j-2,"C",j ;
	ampo += t0*gamma*Cplx_i,"Bdag",j,"Cdag",j,"C",j-2;
	ampo+= t0*gamma*Cplx_i,"B",j, "Cdag",j,"C",j-2 ;
	
	}
    

    return ampo;
  }   
 template<typename siteset>
      auto makeECurr_FT(siteset sites, double t0, double gamma, double omega)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=3;j  <= N-3; j+=2)
        {

      	ampo += t0*t0*Cplx_i,"Cdag",j-2,"C",j+2;
	ampo+= -t0*t0*Cplx_i, "Cdag",j+2,"C",j-2;
	}
	    for(int j=3; j<= N-1; j+=2)
        {
	ampo += -t0*gamma*Cplx_i,"Bdag",j,"Cdag",j-2,"C",j;
	ampo+= -t0*gamma*Cplx_i,"B",j, "Cdag",j-2,"C",j ;
	ampo += t0*gamma*Cplx_i,"Bdag",j,"Cdag",j,"C",j-2;
	ampo+= t0*gamma*Cplx_i,"B",j, "Cdag",j,"C",j-2 ;
	
	}
    

    return ampo;
  }
    template<typename siteset>
    auto makeE_kin(siteset sites, double t0=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N; j+=1)
        {

      	ampo += -t0,"Cdag",j,"C",j+1;
	ampo+= -t0, "Cdag",j+1,"C",j ;
	}

    return ampo;
  }   

    template<typename siteset>
    auto makeE_kin_FT(siteset sites, double t0=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N-1; j+=2)
        {

      	ampo += -t0,"Cdag",j,"C",j+2;
	ampo+= -t0, "Cdag",j+2,"C",j ;
	}

    return ampo;
  }   

    template<typename siteset>
    auto makeE_kin_ph(siteset sites, double omgp=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N; j+=1)
        {

      	ampo += omgp,"Bdag",j,"B",j+1;
	ampo+= omgp, "Bdag",j+1,"B",j ;
	}

    return ampo;
  }   

    template<typename siteset>
    auto makeE_kin_phFT(siteset sites, double omgp=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N-1; j+=2)
        {

      	ampo += omgp,"Bdag",j,"B",j+2;
	ampo+= omgp, "Bdag",j+2,"B",j ;
	}

    return ampo;
  }   
 template<typename siteset>
    auto makeHolstHam_probe(siteset sites, double t0=1, double gamma=1, double omega=1, double factor=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N; j+=1)
        {

      	ampo += -t0*factor,"Cdag",j,"C",j+1;
	ampo+= -t0*std::conj(factor), "Cdag",j+1,"C",j ;
	}
    for(int j=1;j <= N; j += 1)
        {

	   ampo += gamma,"NX",j;
	    ampo += omega,"Nph",j;
        }

    return ampo;
  }
  template<typename siteset>
  auto makeHolstHam(siteset sites, double t0=1, double gamma=1, double omega=1, double eps=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N; j+=1)
        {

      	ampo += -t0,"Cdag",j,"C",j+1;
	ampo+= -t0, "Cdag",j+1,"C",j ;
	}
    for(int j=1;j <= N; j += 1)
        {

	   ampo += gamma,"NX",j;
	    ampo += omega,"Nph",j;
	    ampo += eps,"N",j;
        }

    return ampo;
  }

  template<typename siteset>
  auto makeHolstHaminhom(siteset sites, double t0=1, double gamma=1, double omega=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	for(int j=int(N/2)+1;j  < N; j+=1)
        {

      	ampo += -t0,"Cdag",j,"C",j+1;
	ampo+= -t0, "Cdag",j+1,"C",j ;
	}
	for(int j=int(N/2)+1;j <= N; j += 1)
        {

	   ampo += gamma,"NX",j;
	    ampo += omega,"Nph",j;
        }

    return ampo;
  }
     template<typename siteset>
    auto makeHolstHam_disp(siteset sites, double t0=1, double gamma=1, double omega=1, double omegap=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N; j+=1)
        {

      	ampo += -t0,"Cdag",j,"C",j+1;
	ampo+= -t0, "Cdag",j+1,"C",j ;
	ampo += omegap,"Bdag",j,"B",j+1;
	ampo+= omegap, "Bdag",j+1,"B",j;
	}
    for(int j=1;j <= N; j += 1)
        {

	   ampo += gamma,"NX",j;
	    ampo += omega,"Nph",j;
        }

    return ampo;
  }
    template<typename siteset>
    auto makeHolstHam_disp_probe(siteset sites, double t0=1, double gamma=1, double omega=1, double omegap=1,std::complex<double> factor=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N; j+=1)
        {

      	ampo += -factor*t0,"Cdag",j,"C",j+1;
	ampo+= -std::conj(factor)*t0, "Cdag",j+1,"C",j ;
	ampo += omegap,"Bdag",j,"B",j+1;
	ampo+= omegap, "Bdag",j+1,"B",j;
	}
    for(int j=1;j <= N; j += 1)
        {

	   ampo += gamma,"NX",j;
	    ampo += omega,"Nph",j;
        }

    return ampo;
  }
    template<typename siteset>
    auto makeHolstHamFT(siteset sites, double t0=1, double gamma=1, double omega=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
    {
    int N=length(sites);

        auto ampo = AutoMPO(sites);
    	    for(int j=1;j  <= N-2; j+=2)
        {


      	ampo += -t0,"Cdag",j,"C",j+2;
    	ampo+= -t0, "Cdag",j+2,"C",j ;

    }
    for(int j=1;j < N; j += 2)
        {
    	   ampo += gamma,"NX",j;
	     ampo += omega,"Nph",j;
	 
        }

    return ampo;
  }

 template<typename siteset>
 auto makeHolstHam_dispFT_probe(siteset sites, double t0=1, double gamma=1, double omega=1, double omegap=0, std::complex<double> factor=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
    {
    int N=length(sites);

        auto ampo = AutoMPO(sites);
    	    for(int j=1;j  <= N-2; j+=2)
        {


      	ampo += -factor*t0,"Cdag",j,"C",j+2;
    	ampo+= -std::conj(factor)*t0, "Cdag",j+2,"C",j ;
	
      	ampo += omegap,"Bdag",j,"B",j+2;
    	ampo+= omegap, "Bdag",j+2,"B",j ;

    }
    for(int j=1;j < N; j += 2)
        {
    	   ampo += gamma,"NX",j;
	     ampo += omega,"Nph",j;
	 
        }

   
    
    return ampo;
  }
  // H_p -H_aux
    template<typename siteset>
  auto makeHolstHam_dispFTAux(siteset sites, double t0=1, double gamma=1, double omega=1, double omegap=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
    {
    int N=length(sites);

        auto ampo = AutoMPO(sites);
    	    for(int j=1;j  <= N-2; j+=2)
        {


      	ampo += -t0,"Cdag",j,"C",j+2;
    	ampo+= -t0, "Cdag",j+2,"C",j ;
	
      	ampo += omegap,"Bdag",j,"B",j+2;
    	ampo+= omegap, "Bdag",j+2,"B",j ;

    }
    for(int j=1;j < N; j += 2)
        {
    	   ampo += gamma,"NX",j;
	     ampo += omega,"Nph",j;
	 
        }
    	    for(int j=2;j  < N-1; j+=2)
        {


      	ampo += +t0,"Cdag",j,"C",j+2;
    	ampo+= +t0, "Cdag",j+2,"C",j ;
	
      	ampo += -omegap,"Bdag",j,"B",j+2;
    	ampo+= -omegap, "Bdag",j+2,"B",j ;

    }
     for(int j=2;j <= N; j += 2)
        {
    	   ampo += -gamma,"NX",j;
	     ampo += -omega,"Nph",j;
	 
        }
    
    return ampo;
  }
  // H_p with auxillery space
  template<typename siteset>
  auto makeHolstHam_dispFT(siteset sites, double t0=1, double gamma=1, double omega=1, double omegap=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
    {
    int N=length(sites);

        auto ampo = AutoMPO(sites);
    	    for(int j=1;j  <= N-2; j+=2)
        {


      	ampo += -t0,"Cdag",j,"C",j+2;
    	ampo+= -t0, "Cdag",j+2,"C",j ;
	
      	ampo += omegap,"Bdag",j,"B",j+2;
    	ampo+= omegap, "Bdag",j+2,"B",j ;

    }
    for(int j=1;j < N; j += 2)
        {
    	   ampo += gamma,"NX",j;
	     ampo += omega,"Nph",j;
	 
        }

   
    
    return ampo;
  }
  template<typename siteset>
  auto makeHolstHam_dispFT(siteset sites, double t0=1, double gamma=1, double omega=1, double omegap=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
    {
    int N=length(sites);

        auto ampo = AutoMPO(sites);
    	    for(int j=1;j  <= N-2; j+=2)
        {


      	ampo += -t0,"Cdag",j,"C",j+2;
    	ampo+= -t0, "Cdag",j+2,"C",j ;
	
      	ampo += omegap,"Bdag",j,"B",j+2;
    	ampo+= omegap, "Bdag",j+2,"B",j ;

    }
    for(int j=1;j < N; j += 2)
        {
    	   ampo += gamma,"NX",j;
	     ampo += omega,"Nph",j;
	 
        }

   
    
    return ampo;
  }
  // make H_P -H_A
    template<typename siteset>
  auto makeFTHQ(siteset sites, double t0=1, double gamma=1, double omega=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
    {
    int N=length(sites);
    std::cout<< " N  " << N << std::endl;
        auto ampo = AutoMPO(sites);
    	    for(int j=1;j  <= N-2; j+=2)
        {


      	ampo += -t0,"Cdag",j,"C",j+2;
    	ampo+= -t0, "Cdag",j+2,"C",j ;

    }
    for(int j=1;j <= N; j += 2)
        {
    	   ampo += gamma,"NX",j;
	     ampo += omega,"Nph",j;
        }
	    for(int j=2;j  <= N-1; j+=2)
        {


      	ampo += t0,"Cdag",j,"C",j+2;
    	ampo+= t0, "Cdag",j+2,"C",j ;

    }
    for(int j=2;j <= N; j += 2)
        {
    	   ampo += -gamma,"NX",j;
	     ampo += -omega,"Nph",j;
        }
   
    
    return ampo;
  }
  
  //   template<typename siteset>
  // MPO makePurHolstHam(siteset sites, double t0=1, double gamma=1, double omega=1)
  // {
  //   int N=lenght(sites);
  //       auto ampo = AutoMPO(sites);
  // 	    for(int j=1;j  <= N-2; j+=4)
  //       {

  //     	ampo += -t0,"Cdag",j,"C",j+4;
  // 	ampo+= -t0, "C",j,"Cdag",j+4 ;

  //   }
  //   for(int j=1;j < N; j += 2)
  //       {

  // 	   ampo += gamma,"N",j,"A",j+1;
  // 	  ampo += gamma,"N",j,"Adag",j+1;
  //       }
  //   for(int j = 1; j <= N; j += 4)
  //       {
  // 	   ampo += omega,"N",j+1;

  //       }
  //   auto H = toMPO(ampo);
  //   return H;
  // }
class HolsteinSite;

using Holstein = BasicSiteSet<HolsteinSite>;

class HolsteinSite_up;

using Holstein_up = BasicSiteSet<HolsteinSite_up>;

class HolsteinSite_down;

using Holstein_down = BasicSiteSet<HolsteinSite_down>;
  
  //using HolsteinMixed = MixedSiteSet<FermionSite,BosonSite>;

class HolsteinSite
    {
      Index s;


      
      std::vector<int> mvec;
      std::vector<Index> indexvec;
    public:

      HolsteinSite(Index I): s(I) { }

      //HolsteinSite(IQIndex I, int Mm=4) : M(Mm), s(I)  { } // not quite safe, how to control f the index contains right phonon number

      HolsteinSite( Args const& args = Args::global() )
        {
        auto conserveQNs = args.getBool("ConserveQNs",true);
        // auto conserveNb = args.getBool("ConserveNb",conserveQNs);
        auto conserve_Nf = args.getBool("ConserveNf",conserveQNs);
	        auto oddevenupdown = args.getBool("OddEvenUpDown",false);
	auto tags = TagSet("Site,Hol");
        auto n = 1;
        if(args.defined("SiteNumber") )
            {
            n = args.getInt("SiteNumber");
            tags.addTags("n="+str(n));
            }
	auto diffMaxOcc = args.getBool("DiffMaxOcc",false);
	auto maxOcc{0};
	if(diffMaxOcc)
	  {
	    auto occVec=args.getVecInt("MaxOccVec");
	    maxOcc= occVec[n-1];
	  }
	else{
        maxOcc = args.getInt("MaxOcc",1);
	}
	        if(not conserveQNs)
            {
	      s = Index(2*(maxOcc+1),tags);
            }
        else if(not oddevenupdown) //usual case
            {

	      auto q_occ = QN({"Nf",1, -1});
            if(not conserve_Nf) q_occ = QN({"Pf",1,-2});
            s = Index(QN({"Nf",0,-1}),(maxOcc+1),
                      q_occ,(maxOcc+1),Out,tags);
            }
        else
            {
	      auto q_emp = QN({"Sz",0},{"Nf",0,-1});
            QN q_occ;
            if(n%2==1) q_occ = QN({"Sz",+1},{"Nf",1,-1});
            else       q_occ = QN({"Sz",-1},{"Nf",1,-1});
            s = Index(q_emp,(maxOcc+1),
                      q_occ,(maxOcc+1),Out,tags);
            }
	}
        // if(conserveQNs)
        //     {
        //     if(conserveNb)
        //         {
        //         auto qints = Index::qnstorage(1+maxOcc);
        //         for(int n : range(1+maxOcc)) 
        //             {
        //             qints[n] = QNInt(QN({"Nb",n}),1);
        //             }
        //         s = Index(std::move(qints),tags);
        //         }
        //     else
        //         {
        //         s = Index(QN(),1+maxOcc,tags);
        //         }
        //     }
        // else
        //     {
        //     if(conserveNb) Error("ConserveNb cannot be true when ConserveQNs=false");
        //     s = Index(1+maxOcc,tags);
        //     }
        // }


    Index
    index() const { return s; }

    IndexVal
    state(std::string const& state)
        {
	  auto d = static_cast<int>(dim(s)/2);
        if(state == "Emp" || state == "0") 
            {
            return s(1);
            }
        else 
        if(state == "Occ" || state == "1") 
            {
            return s(d+1);
            }
	 if(state == "EmpPh") 
            {
            return s(2);
            }
        else 
        if(state == "OccPh") 
            {
            return s(d+2);
            }
	 	 if(state == "EmpPh2") 
            {
            return s(3);
            }
        else 
        if(state == "OccPh2") 
            {
            return s(d+3);
            }
        else
            {
            Error("State " + state + " not recognized");
            }
        return IndexVal{};
        }

	ITensor
	op(std::string const& opname,
	   Args const& args) const
        {
        auto sP = prime(s);
	auto d = static_cast<int>(dim(s)/2);
	auto Op = ITensor(dag(s),sP);
	

        if(opname == "N" || opname == "n")
            {

	      for (auto n :range1(d))
		{
		
		  Op.set(s=(d+n),sP=(d+n),1);
	      }
            }
	else
	  if(opname == "Nph")
	    {


	       
	      for(auto n :range1(d))
		{
	
		 Op.set(s=n, sP=n, n-1);
		 Op.set(s=(n+d), sP=(n+d), n-1);
 	      }
	    }
	  else
	     if(opname == "X" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,std::sqrt(n));
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=1+n,sP=n,std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "Bdag" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,std::sqrt(n));
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "B" )
            {

            for(auto n : range1(d-1))
                {
                Op.set(s=1+n,sP=n,std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
                }
	        if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
 	      
            }
	   else
	  if(opname == "Bdagnorm" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,1);
		Op.set(s=(d+n),sP=(d+1+n),1);
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "Bnorm" )
            {

            for(auto n : range1(d-1))
                {
                Op.set(s=1+n,sP=n,1);
		Op.set(s=(d+n+1),sP=(d+n),1);
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
	      
 	      
            }
	  else
	    	  if(opname == "NX" )
            {


            for(auto n : range1(d-1))
                {
		  
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
		
                }
	     if(d-1==0)
	       {

		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
		  if(opname == "NBdag" )
            {


            for(auto n : range1(d-1))
                {
		  
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
                }
	     if(d-1==0)
	       {

		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "NB" )
            {

            for(auto n : range1(d-1))
                {
                
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
                }
	     if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
 	      
            }
        else
        if(opname == "C")
            {
	      
 
		for(auto n : range1(d))
		  {
 
		 Op.set(s=(d+n), sP=(n), 1);
	      }

            }
        else
        if(opname == "Cdag")
            {

		for(auto n : range1(d))
                {

		Op.set( s=n, sP=(d+n), 1);
	      }

            }
        else
        if(opname == "A")
	  {

		for(auto n : range1(d))
                {
	
		 Op.set(s=(d+n), sP=(n), 1);
	      }

	    
            }
        else
        if(opname == "Adag")
            {

		for(auto n : range1(d))
                {
	
		Op.set( s=n, sP=(d+n), 1);
	      }	      

            }
        else
        if(opname == "F" || opname == "FermiPhase")
            {

	      		for(auto n : range1(d))
                {

            Op.set(s=n,sP=n,1);
            Op.set(s=(d+n), sP=(d+n),-1);
	      }

            }
        else
        if(opname == "projEmp")
            {
	      	for(auto n : range1(d))
		  {
            Op.set(s=n,sP=n,1);
		  }
            }
        else
        if(opname == "projOcc")
            {
	      	for(auto n : range1(d))
		  {
		    Op.set(s=(d+n),sP=(d+n),1); 
            }
	    }
        else
	  if(opname == "maxPhProj")
            {
	      	// for(auto n : range1(d))
		//   {
            Op.set(s=d,sP=d,1);
	    Op.set(s=2*d,sP=2*d,1);
	    //  }
            }
        else
            {
            Error("Operator \"" + opname + "\" name not recognized");
            }

        return Op;
        }

      HolsteinSite(int n, Args const& args = Args::global())
      {
        *this = HolsteinSite({args,"SiteNumber=",n});
      }
};





  // holstein up
class HolsteinSite_up
    {
      Index s;


      
      std::vector<int> mvec;
      std::vector<Index> indexvec;
    public:

      HolsteinSite_up(Index I): s(I) { }

      //HolsteinSite(IQIndex I, int Mm=4) : M(Mm), s(I)  { } // not quite safe, how to control f the index contains right phonon number

      HolsteinSite_up( Args const& args = Args::global() )
        {
        auto conserveQNs = args.getBool("ConserveQNs",true);
        // auto conserveNb = args.getBool("ConserveNb",conserveQNs);
        auto conserve_Nf = args.getBool("ConserveNf",conserveQNs);
	        auto oddevenupdown = args.getBool("OddEvenUpDown",false);
	auto tags = TagSet("Site,Hol");
        auto n = 1;
        if(args.defined("SiteNumber") )
            {
            n = args.getInt("SiteNumber");
            tags.addTags("n="+str(n));
            }
	auto diffMaxOcc = args.getBool("DiffMaxOcc",false);
	auto maxOcc{0};
	if(diffMaxOcc)
	  {
	    auto occVec=args.getVecInt("MaxOccVec");
	    maxOcc= occVec[n-1];
	  }
	else{
        maxOcc = args.getInt("MaxOcc",1);
	}
	        if(not conserveQNs)
            {
	      s = Index(2*(maxOcc+1),tags);
            }
        else if(not oddevenupdown) //usual case
            {

	      auto q_occ = QN({"Nf",1, -1},{"Sz",+1});
            if(not conserve_Nf) q_occ = QN({"Pf",1,-2});
            s = Index(QN({"Nf",0,-1}),(maxOcc+1),
                      q_occ,(maxOcc+1),Out,tags);
            }
        else
            {
            QN q_occ;
	    auto q_emp = QN({"Sz",0},{"Nf",0,-1});
            if(n%2==1) q_occ = QN({"Sz",+1},{"Nf",1,-1});
            else       q_occ = QN({"Sz",-1},{"Nf",1,-1});
            s = Index(q_emp,(maxOcc+1),
                      q_occ,(maxOcc+1),Out,tags);
            }
	}
        // if(conserveQNs)
        //     {
        //     if(conserveNb)
        //         {
        //         auto qints = Index::qnstorage(1+maxOcc);
        //         for(int n : range(1+maxOcc)) 
        //             {
        //             qints[n] = QNInt(QN({"Nb",n}),1);
        //             }
        //         s = Index(std::move(qints),tags);
        //         }
        //     else
        //         {
        //         s = Index(QN(),1+maxOcc,tags);
        //         }
        //     }
        // else
        //     {
        //     if(conserveNb) Error("ConserveNb cannot be true when ConserveQNs=false");
        //     s = Index(1+maxOcc,tags);
        //     }
        // }


    Index
    index() const { return s; }

    IndexVal
    state(std::string const& state)
        {
	  auto d = static_cast<int>(dim(s)/2);
        if(state == "Emp" || state == "0") 
            {
            return s(1);
            }
        else 
        if(state == "Occ" || state == "1") 
            {
            return s(d+1);
            }
	 if(state == "EmpPh") 
            {
            return s(2);
            }
        else 
        if(state == "OccPh") 
            {
            return s(d+2);
            }
	 	 if(state == "EmpPh2") 
            {
            return s(3);
            }
        else 
        if(state == "OccPh2") 
            {
            return s(d+3);
            }
        else
            {
            Error("State " + state + " not recognized");
            }
        return IndexVal{};
        }

	ITensor
	op(std::string const& opname,
	   Args const& args) const
        {
        auto sP = prime(s);
	auto d = static_cast<int>(dim(s)/2);
	auto Op = ITensor(dag(s),sP);
	

        if(opname == "N" || opname == "n")
            {

	      for (auto n :range1(d))
		{
		
		  Op.set(s=(d+n),sP=(d+n),1);
	      }
            }
	else
	  if(opname == "Nph")
	    {


	       
	      for(auto n :range1(d))
		{
	
		 Op.set(s=n, sP=n, n-1);
		 Op.set(s=(n+d), sP=(n+d), n-1);
 	      }
	    }
	  else
	     if(opname == "X" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,std::sqrt(n));
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=1+n,sP=n,std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "Bdag" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,std::sqrt(n));
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "B" )
            {

            for(auto n : range1(d-1))
                {
                Op.set(s=1+n,sP=n,std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
                }
	        if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
 	      
            }
	   else
	  if(opname == "Bdagnorm" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,1);
		Op.set(s=(d+n),sP=(d+1+n),1);
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "Bnorm" )
            {

            for(auto n : range1(d-1))
                {
                Op.set(s=1+n,sP=n,1);
		Op.set(s=(d+n+1),sP=(d+n),1);
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
	      
 	      
            }
	  else
	    	  if(opname == "NX" )
            {


            for(auto n : range1(d-1))
                {
		  
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
		
                }
	     if(d-1==0)
	       {

		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
		  if(opname == "NBdag" )
            {


            for(auto n : range1(d-1))
                {
		  
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
                }
	     if(d-1==0)
	       {

		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "NB" )
            {

            for(auto n : range1(d-1))
                {
                
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
                }
	     if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
 	      
            }
        else
        if(opname == "C")
            {
	      
 
		for(auto n : range1(d))
		  {
 
		 Op.set(s=(d+n), sP=(n), 1);
	      }

            }
        else
        if(opname == "Cdag")
            {

		for(auto n : range1(d))
                {

		Op.set( s=n, sP=(d+n), 1);
	      }

            }
        else
        if(opname == "A")
	  {

		for(auto n : range1(d))
                {
	
		 Op.set(s=(d+n), sP=(n), 1);
	      }

	    
            }
        else
        if(opname == "Adag")
            {

		for(auto n : range1(d))
                {
	
		Op.set( s=n, sP=(d+n), 1);
	      }	      

            }
        else
        if(opname == "F" || opname == "FermiPhase")
            {

	      		for(auto n : range1(d))
                {

            Op.set(s=n,sP=n,1);
            Op.set(s=(d+n), sP=(d+n),-1);
	      }

            }
        else
        if(opname == "projEmp")
            {
	      	for(auto n : range1(d))
		  {
            Op.set(s=n,sP=n,1);
		  }
            }
        else
        if(opname == "projOcc")
            {
	      	for(auto n : range1(d))
		  {
		    Op.set(s=(d+n),sP=(d+n),1); 
            }
	    }
        else
	  if(opname == "maxPhProj")
            {
	      	// for(auto n : range1(d))
		//   {
            Op.set(s=d,sP=d,1);
	    Op.set(s=2*d,sP=2*d,1);
	    //  }
            }
        else
            {
            Error("Operator \"" + opname + "\" name not recognized");
            }

        return Op;
        }

      HolsteinSite_up(int n, Args const& args = Args::global())
      {
        *this = HolsteinSite_up({args,"SiteNumber=",n});
      }
};



  // down
  // holstein up
class HolsteinSite_down
    {
      Index s;


      
      std::vector<int> mvec;
      std::vector<Index> indexvec;
    public:

      HolsteinSite_down(Index I): s(I) { }

      //HolsteinSite(IQIndex I, int Mm=4) : M(Mm), s(I)  { } // not quite safe, how to control f the index contains right phonon number

      HolsteinSite_down( Args const& args = Args::global() )
        {
        auto conserveQNs = args.getBool("ConserveQNs",true);
        // auto conserveNb = args.getBool("ConserveNb",conserveQNs);
        auto conserve_Nf = args.getBool("ConserveNf",conserveQNs);
	        auto oddevenupdown = args.getBool("OddEvenUpDown",false);
	auto tags = TagSet("Site,Hol");
        auto n = 1;
        if(args.defined("SiteNumber") )
            {
            n = args.getInt("SiteNumber");
            tags.addTags("n="+str(n));
            }
	auto diffMaxOcc = args.getBool("DiffMaxOcc",false);
	auto maxOcc{0};
	if(diffMaxOcc)
	  {
	    auto occVec=args.getVecInt("MaxOccVec");
	    maxOcc= occVec[n-1];
	  }
	else{
        maxOcc = args.getInt("MaxOcc",1);
	}
	        if(not conserveQNs)
            {
	      s = Index(2*(maxOcc+1),tags);
            }
        else if(not oddevenupdown) //usual case
            {

	      auto q_occ = QN({"Nf",1, -1},{"Sz",-1});
            if(not conserve_Nf) q_occ = QN({"Pf",1,-2});
            s = Index(QN({"Nf",0,-1}),(maxOcc+1),
                      q_occ,(maxOcc+1),Out,tags);
            }
        else
            {
            QN q_occ;
	    auto q_emp = QN({"Sz",0},{"Nf",0,-1});
            if(n%2==1) q_occ = QN({"Sz",+1},{"Nf",1,-1});
            else       q_occ = QN({"Sz",-1},{"Nf",1,-1});
            s = Index(q_emp,(maxOcc+1),
                      q_occ,(maxOcc+1),Out,tags);
            }
	}
        // if(conserveQNs)
        //     {
        //     if(conserveNb)
        //         {
        //         auto qints = Index::qnstorage(1+maxOcc);
        //         for(int n : range(1+maxOcc)) 
        //             {
        //             qints[n] = QNInt(QN({"Nb",n}),1);
        //             }
        //         s = Index(std::move(qints),tags);
        //         }
        //     else
        //         {
        //         s = Index(QN(),1+maxOcc,tags);
        //         }
        //     }
        // else
        //     {
        //     if(conserveNb) Error("ConserveNb cannot be true when ConserveQNs=false");
        //     s = Index(1+maxOcc,tags);
        //     }
        // }


    Index
    index() const { return s; }

    IndexVal
    state(std::string const& state)
        {
	  auto d = static_cast<int>(dim(s)/2);
        if(state == "Emp" || state == "0") 
            {
            return s(1);
            }
        else 
        if(state == "Occ" || state == "1") 
            {
            return s(d+1);
            }
	 if(state == "EmpPh") 
            {
            return s(2);
            }
        else 
        if(state == "OccPh") 
            {
            return s(d+2);
            }
	 	 if(state == "EmpPh2") 
            {
            return s(3);
            }
        else 
        if(state == "OccPh2") 
            {
            return s(d+3);
            }
        else
            {
            Error("State " + state + " not recognized");
            }
        return IndexVal{};
        }

	ITensor
	op(std::string const& opname,
	   Args const& args) const
        {
        auto sP = prime(s);
	auto d = static_cast<int>(dim(s)/2);
	auto Op = ITensor(dag(s),sP);
	

        if(opname == "N" || opname == "n")
            {

	      for (auto n :range1(d))
		{
		
		  Op.set(s=(d+n),sP=(d+n),1);
	      }
            }
	else
	  if(opname == "Nph")
	    {


	       
	      for(auto n :range1(d))
		{
	
		 Op.set(s=n, sP=n, n-1);
		 Op.set(s=(n+d), sP=(n+d), n-1);
 	      }
	    }
	  else
	     if(opname == "X" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,std::sqrt(n));
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=1+n,sP=n,std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "Bdag" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,std::sqrt(n));
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "B" )
            {

            for(auto n : range1(d-1))
                {
                Op.set(s=1+n,sP=n,std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
                }
	        if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
 	      
            }
	   else
	  if(opname == "Bdagnorm" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,1);
		Op.set(s=(d+n),sP=(d+1+n),1);
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "Bnorm" )
            {

            for(auto n : range1(d-1))
                {
                Op.set(s=1+n,sP=n,1);
		Op.set(s=(d+n+1),sP=(d+n),1);
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
	      
 	      
            }
	  else
	    	  if(opname == "NX" )
            {


            for(auto n : range1(d-1))
                {
		  
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
		
                }
	     if(d-1==0)
	       {

		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
		  if(opname == "NBdag" )
            {


            for(auto n : range1(d-1))
                {
		  
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
                }
	     if(d-1==0)
	       {

		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "NB" )
            {

            for(auto n : range1(d-1))
                {
                
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
                }
	     if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
 	      
            }
        else
        if(opname == "C")
            {
	      
 
		for(auto n : range1(d))
		  {
 
		 Op.set(s=(d+n), sP=(n), 1);
	      }

            }
        else
        if(opname == "Cdag")
            {

		for(auto n : range1(d))
                {

		Op.set( s=n, sP=(d+n), 1);
	      }

            }
        else
        if(opname == "A")
	  {

		for(auto n : range1(d))
                {
	
		 Op.set(s=(d+n), sP=(n), 1);
	      }

	    
            }
        else
        if(opname == "Adag")
            {

		for(auto n : range1(d))
                {
	
		Op.set( s=n, sP=(d+n), 1);
	      }	      

            }
        else
        if(opname == "F" || opname == "FermiPhase")
            {

	      		for(auto n : range1(d))
                {

            Op.set(s=n,sP=n,1);
            Op.set(s=(d+n), sP=(d+n),-1);
	      }

            }
        else
        if(opname == "projEmp")
            {
	      	for(auto n : range1(d))
		  {
            Op.set(s=n,sP=n,1);
		  }
            }
        else
        if(opname == "projOcc")
            {
	      	for(auto n : range1(d))
		  {
		    Op.set(s=(d+n),sP=(d+n),1); 
            }
	    }
        else
	  if(opname == "maxPhProj")
            {
	      	// for(auto n : range1(d))
		//   {
            Op.set(s=d,sP=d,1);
	    Op.set(s=2*d,sP=2*d,1);
	    //  }
            }
        else
            {
            Error("Operator \"" + opname + "\" name not recognized");
            }

        return Op;
        }

      HolsteinSite_down(int n, Args const& args = Args::global())
      {
        *this = HolsteinSite_down({args,"SiteNumber=",n});
      }
};




    template<typename T>
  struct is_Mixed_hol: std::false_type {
  };
 using  Holstein_exp = MixedSiteSet<HolsteinSite_down,HolsteinSite_up>;
  template<>
  struct is_Mixed_hol<Holstein_exp>: std::true_type {
   
  };





} //namespace itensor
#endif /* HOLSTEIN_H */
