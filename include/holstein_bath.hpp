//
// Distributed under the ITensor Library License, Version 1.2
//    (See accompanying LICENSE file.)
//
 #ifndef HOLSTEIN_H
#define HOLSTEIN_H
#include "itensor/mps/siteset.h"
#include "itensor/all.h"
#include"extra.hpp"
namespace itensor {

  template<typename siteset>
auto makeHolstHam_bath(siteset sites, double t0=1, double gamma=1, double omega=1)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    int N=length(sites);
        auto ampo = AutoMPO(sites);
	    for(int j=1;j  < N-2; j+=2)
        {

	  	ampo += -t0,"Cdag",j,"C",j+2;
	  ampo+= -t0, "Cdag",j+2,"C",j ;
	}
    for(int j=1;j <= N-1; j += 2)
        {
	  std::cout<< "j "<<gamma << "  "<< j << std::endl;
	  ampo += gamma,"NB",j, "Bdag_beta", j+1;
	  ampo += gamma,"NBdag",j, "B_beta", j+1;
	   ampo += omega,"Nph",j;

        }

    return ampo;
  }
//     template<typename siteset>
//     auto makeHolstHam_disp(siteset sites, double t0=1, double gamma=1, double omega=1, double t1=1)
// ->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
//   {
//     int N=length(sites);
//         auto ampo = AutoMPO(sites);
// 	    for(int j=1;j  < N; j+=1)
//         {

//       	ampo += -t0,"Cdag",j,"C",j+1;
// 	ampo+= -t0, "Cdag",j+1,"C",j ;
// 	ampo += -t1,"Bdag",j,"B",j+1;
// 	ampo+= -t1, "Bdag",j+1,"B",j;
// 	}
//     for(int j=1;j <= N; j += 1)
//         {

// 	   ampo += gamma,"NX",j;
// 	    ampo += omega,"Nph",j;
//         }

//     return ampo;
//   }
//     template<typename siteset>
//   auto makePureHolstHam(siteset sites, double t0=1, double gamma=1, double omega=1)
// ->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
//     {
//     int N=length(sites);
//     std::cout<< " N  " << N << std::endl;
//         auto ampo = AutoMPO(sites);
//     	    for(int j=1;j  <= N-2; j+=2)
//         {


//       	ampo += -t0,"Cdag",j,"C",j+2;
//     	ampo+= -t0, "Cdag",j+2,"C",j ;

//     }
//     for(int j=1;j <= N; j += 2)
//         {
//     	   ampo += gamma,"NX",j;
// 	     ampo += omega,"Nph",j;
//         }
   
    
//     return ampo;
//   }
//   // make H_P -H_A
//     template<typename siteset>
//   auto makeFTHQ(siteset sites, double t0=1, double gamma=1, double omega=1)
// ->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
//     {
//     int N=length(sites);
//     std::cout<< " N  " << N << std::endl;
//         auto ampo = AutoMPO(sites);
//     	    for(int j=1;j  <= N-2; j+=2)
//         {


//       	ampo += -t0,"Cdag",j,"C",j+2;
//     	ampo+= -t0, "Cdag",j+2,"C",j ;

//     }
//     for(int j=1;j <= N; j += 2)
//         {
//     	   ampo += gamma,"NX",j;
// 	     ampo += omega,"Nph",j;
//         }
// 	    for(int j=2;j  <= N-1; j+=2)
//         {


//       	ampo += t0,"Cdag",j,"C",j+2;
//     	ampo+= t0, "Cdag",j+2,"C",j ;

//     }
//     for(int j=2;j <= N; j += 2)
//         {
//     	   ampo += -gamma,"NX",j;
// 	     ampo += -omega,"Nph",j;
//         }
   
    
//     return ampo;
//   }
  
//   //   template<typename siteset>
//   // MPO makePurHolstHam(siteset sites, double t0=1, double gamma=1, double omega=1)
//   // {
//   //   int N=lenght(sites);
//   //       auto ampo = AutoMPO(sites);
//   // 	    for(int j=1;j  <= N-2; j+=4)
//   //       {

//   //     	ampo += -t0,"Cdag",j,"C",j+4;
//   // 	ampo+= -t0, "C",j,"Cdag",j+4 ;

//   //   }
//   //   for(int j=1;j < N; j += 2)
//   //       {

//   // 	   ampo += gamma,"N",j,"A",j+1;
//   // 	  ampo += gamma,"N",j,"Adag",j+1;
//   //       }
//   //   for(int j = 1; j <= N; j += 4)
//   //       {
//   // 	   ampo += omega,"N",j+1;

//   //       }
//   //   auto H = toMPO(ampo);
//   //   return H;
//   // }
class HolsteinSite_bath;

using Holstein_bath = BasicSiteSet<HolsteinSite_bath>;
  

class HolsteinSite_bath
    {
      Index s;

    public:

      HolsteinSite_bath(Index I): s(I) { }

      //HolsteinSite(IQIndex I, int Mm=4) : M(Mm), s(I)  { } // not quite safe, how to control f the index contains right phonon number

      HolsteinSite_bath( Args const& args = Args::global() )
        {
        auto conserveQNs = args.getBool("ConserveQNs",true);
        auto conserveNb = args.getBool("ConserveNb",true);
        auto conserve_Nf = args.getBool("ConserveNf",conserveQNs);
	auto oddevenupdown = args.getBool("OddEvenUpDown",false);
	auto tags = TagSet("Site,Hol");
        auto n = 1;
        if(args.defined("SiteNumber") )
            {
            n = args.getInt("SiteNumber");
            tags.addTags("n="+str(n));
            }
	auto diffMaxOcc = args.getBool("DiffMaxOcc",false);
	auto maxOcc{0};
	if(diffMaxOcc)
	  {
	    auto occVec=args.getVecInt("MaxOccVec");
	    maxOcc= occVec[n-1];
	  }
	else{
        maxOcc = args.getInt("MaxOcc",1);
	}
	        if(not conserveQNs)
            {
	      s = Index(2*(maxOcc+1),tags);
            }
        else if(not oddevenupdown) //usual case
            {
	      auto q_occ = QN({"Nf",1, -1});
            if(not conserve_Nf) q_occ = QN({"Pf",1,-2});
	    // auto qints = Index::qnstorage(1+maxOcc);
	    auto qints = Index::qnstorage(2*(1+maxOcc));
	    int bos_nr=0;
	    for(int n=0; n<(1+maxOcc); n++) 
                    {
		      
		      qints[n] = QNInt(QN({"Nb",bos_nr},{"Nf",0,-1}),1);
		      bos_nr++;
		    }
		    bos_nr=0;
	        for(int n=(1+maxOcc); n<2*(1+maxOcc); n++) 
                    {
		      
		      qints[n] = QNInt(QN({"Nb",bos_nr},{"Nf",1,-1}),1);
		      bos_nr++;
		    }
s = Index(std::move(qints),tags);
            }
       
	}
        // if(conserveQNs)
        //     {
        //     if(conserveNb)
        //         {
        //         auto qints = Index::qnstorage(1+maxOcc);
        //         for(int n : range(1+maxOcc)) 
        //             {
        //             qints[n] = QNInt(QN({"Nb",n}),1);
        //             }
        //         s = Index(std::move(qints),tags);
        //         }
        //     else
        //         {
        //         s = Index(QN(),1+maxOcc,tags);
        //         }
        //     }
        // else
        //     {
        //     if(conserveNb) Error("ConserveNb cannot be true when ConserveQNs=false");
        //     s = Index(1+maxOcc,tags);
        //     }
        // }


    Index
    index() const { return s; }

    IndexVal
    state(std::string const& state)
        {
	  auto d = static_cast<int>(dim(s)/2);

	  std::vector<std::string> numbers;
	  for(int i=1; i<=dim(s); i++)
	    {
	      numbers.push_back(std::to_string(i));
	    }
	  if(std::find(numbers.begin(), numbers.end(), state) != numbers.end())
	    {
	     return s(std::stoi(state));
	    }
	  else
        if(state == "Emp" ) 
            {
            return s(1);
            }
        else 
        if(state == "Occ" ) 
            {
            return s(d+1);
            }
	else
	  if(state == "OccFullPh" ) 
            {
            return s(dim(s));
            }
	  else
	  if(state == "EmpFullPh" ) 
            {
	      return s(d);
            }
	  else
	  if(state == "EmpFullPhmin1" ) 
            {
	      return s(d-1);
            }
	else
	 if(state == "EmpPh") 
            {
            return s(2);
            }
        else 
        if(state == "OccPh") 
            {
            return s(d+2);
            }
	else
	 	 if(state == "EmpPh2") 
            {
            return s(3);
            }
        else 
        if(state == "OccPh2") 
            {
            return s(d+3);
            }
        else
            {
            Error("State " + state + " not recognized");
            }
        return IndexVal{};
        }

	ITensor
	op(std::string const& opname,
	   Args const& args) const
        {
        auto sP = prime(s);
	auto d = static_cast<int>(dim(s)/2);
	auto Op = ITensor(dag(s),sP);
	

        if(opname == "N" || opname == "n")
            {

	      for (auto n :range1(d))
		{
		
		  Op.set(s=(d+n),sP=(d+n),1);
	      }
            }
	else
	  if(opname == "Nph")
	    {


	       
	      for(auto n :range1(d))
		{
	
		 Op.set(s=n, sP=n, n-1);
		 Op.set(s=(n+d), sP=(n+d), n-1);
 	      }
	    }
		else
	  if(opname == "Nph_beta")
	    {


	       
	      for(auto n :range1(d))
		{
	
		 Op.set(s=n, sP=n, 1.);
		 Op.set(s=(n+d), sP=(n+d), 1.);
 	      }
	    }
	  else
	     if(opname == "X" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,std::sqrt(n));
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=1+n,sP=n,std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	 else
	     if(opname == "X_beta" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,1.);
		Op.set(s=(d+n),sP=(d+1+n),1.);
		Op.set(s=1+n,sP=n,1.);
		Op.set(s=(d+n+1),sP=(d+n),1.);
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "Bdag" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,std::sqrt(n));
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "B" )
            {

            for(auto n : range1(d-1))
                {
                Op.set(s=1+n,sP=n,std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
                }
	        if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
 	      
            }
	else
		  if(opname == "Bdag_beta" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,1);
		Op.set(s=(d+n),sP=(d+1+n),1);
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "B_beta" )
            {

            for(auto n : range1(d-1))
                {
                Op.set(s=1+n,sP=n,1.);
		Op.set(s=(d+n+1),sP=(d+n),1.);
                }
	        if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
 	      
            }
	   else
	  if(opname == "Bdagnorm" )
            {


            for(auto n : range1(d-1))
                {
                Op.set(s=n,sP=1+n,1);
		Op.set(s=(d+n),sP=(d+1+n),1);
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "Bnorm" )
            {

            for(auto n : range1(d-1))
                {
                Op.set(s=1+n,sP=n,1);
		Op.set(s=(d+n+1),sP=(d+n),1);
                }
	      if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
	      
 	      
            }
	  else
	    	  if(opname == "NX" )
            {


            for(auto n : range1(d-1))
                {
		  
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
		
                }
	     if(d-1==0)
	       {

		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
		  if(opname == "NBdag" )
            {


            for(auto n : range1(d-1))
                {
		  
		Op.set(s=(d+n),sP=(d+1+n),std::sqrt(n));
                }
	     if(d-1==0)
	       {

		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "NB" )
            {

            for(auto n : range1(d-1))
                {
                
		Op.set(s=(d+n+1),sP=(d+n),std::sqrt(n));
                }
	     if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
 	      
            }
	else
		  if(opname == "NBdag_beta" )
            {


            for(auto n : range1(d-1))
                {
		  
		Op.set(s=(d+n),sP=(d+1+n),1);
                }
	     if(d-1==0)
	       {

		 Op.set(s=(1),sP=(1),0);
	       }

            }
	else
	  if(opname == "NB_beta" )
            {

            for(auto n : range1(d-1))
                {
                
		Op.set(s=(d+n+1),sP=(d+n),1);
                }
	     if(d-1==0)
	       {
		 Op.set(s=(1),sP=(1),0);
	       }
 	      
            }
        else
        if(opname == "C")
            {
	      
 
		for(auto n : range1(d))
		  {
 
		 Op.set(s=(d+n), sP=(n), 1);
	      }

            }
        else
        if(opname == "Cdag")
            {

		for(auto n : range1(d))
                {

		Op.set( s=n, sP=(d+n), 1);
	      }

            }
        else
        if(opname == "A")
	  {

		for(auto n : range1(d))
                {
	
		 Op.set(s=(d+n), sP=(n), 1);
	      }

	    
            }
        else
        if(opname == "Adag")
            {

		for(auto n : range1(d))
                {
	
		Op.set( s=n, sP=(d+n), 1);
	      }	      

            }
        else
        if(opname == "F" || opname == "FermiPhase")
            {

	      		for(auto n : range1(d))
                {

            Op.set(s=n,sP=n,1);
            Op.set(s=(d+n), sP=(d+n),-1);
	      }

            }
        else
        if(opname == "projEmp")
            {
	      	for(auto n : range1(d))
		  {
            Op.set(s=n,sP=n,1);
		  }
            }
        else
        if(opname == "projOcc")
            {
	      	for(auto n : range1(d))
		  {
		    Op.set(s=(d+n),sP=(d+n),1); 
            }
	    }
        else
	  if(opname == "maxPhProj")
            {
	      	// for(auto n : range1(d))
		//   {
            Op.set(s=d,sP=d,1);
	    Op.set(s=2*d,sP=2*d,1);
	    //  }
            }
        else
            {
            Error("Operator \"" + opname + "\" name not recognized");
            }

        return Op;
        }
};

} //namespace itensor
#endif /* HOLSTEIN_H */
