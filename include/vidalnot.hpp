#ifndef VIDALNOT2_H
#define VIDALNOT2_H
#include "itensor/mps/mpo.h"
#include<map>
#include<type_traits>
//using namespace itensor;
/*
making an mpo in vidals notation
*/
namespace itensor{
  const double cut=1E-8;

class vMPS
   {
   public:

     MPS& psi;
     // this causes somwhat confusing notation at first but is better once you get used to it Ls[i] gets site i, not ith elemnets
     // convetion
     // \Labda_i comes from A_i+1= \lamda_i \Gamma_i+1
     std::map<int, ITensor> Ls; // Lambdas
     int sites;


 void makevMPS(){
       psi.position(1);
      	 auto B=psi.A(1);
      	 auto x = findIndex(B,"Site");
      	 auto y=findIndex(B, "Link");
      	 ITensor A(x), V, D;
	 
      	 svd(B,A,D,V,{"Cutoff", cut});

     	 auto Dinv=D;
	 
     	 D*=1./norm(D);
     	 Ls[1]= (D);
     	  psi.setA(1, A);
	 
     	 for(int i=2; i  <length(psi); i++)
      	    {
      	      B=psi.A(i);
      	      auto M=D*V*B;

      	      auto x = findIndex(B,"Site");
      	      auto y=commonIndex(M, Ls[i-1], "Link");
      	       A=ITensor(x, y);     
      	      svd(M,A,D,V,{"Cutoff",cut});
     	      //   D/=norm(D);
     	      	 D*=1./norm(D);
     	      Ls[i]=(D);
      	      psi.setA(i, A);
     	    }
     	  // std::cout<< "here "<< std::endl;
      	  B=psi.A(length(psi));
     	  A=D*V*B;

     	  //std::cout<< "here "<< std::endl;
     	  psi.setA(length(psi), A);

     	  assert(Ls.size()==length(psi));
	 
     } 
     vMPS(MPS& psi): psi(psi), sites(length(psi))
       {
	 makevMPS();
       }
     //  int N()const {return sites;}
     void position(int i){psi.position(i);}
     ITensor& Aref(int i)const {return psi.Aref(i);}
     const ITensor& A(int i)const {return psi.A(i);}
     void setA(int i, ITensor U ) {psi.setA(i, U);}
        // notation i gives acces to lmda belonging to A_i=L_{i-1}G_i
   
     void set_centerN(){
       for(int i=1; i<sites-1; i++)
	 {
	   auto AA = psi.A(i)*psi.A(i+1);
	   			  ITensor D;
    svd(AA*Ls[i+1],psi.Aref(i),D,psi.Aref(i+1) ,{"Cutoff",cut});
    Ls[i]=(D);
        psi.setA(i+1, dag(psi.A(i))*AA);
	 }
       	   auto AA = psi.A(sites-1)*psi.A(sites);
	   			  ITensor D;
    svd(AA,psi.Aref(sites-1),D,psi.Aref(sites) ,{"Cutoff",cut});
    Ls[sites-1]=(D);
        psi.setA(sites, dag(psi.A(sites-1))*AA);

	
     }
   };
  int length(vMPS psi)
  {return length(psi.psi);}
   auto  localEval(int j, vMPS& psi, ITensor Op)->double
{


  auto ket = psi.A(j);
  if(j!=length(psi))
    {ket*=psi.Ls[j];}
         auto bra = dag(prime(ket,"Site"));

        auto eVal = eltC(bra*Op*ket);
	return eVal.real();
}
  
  auto  localEval(int j, MPS& psi, ITensor Op)->Cplx
{
  psi.position(j);

        auto ket = psi(j);
        auto bra = dag(prime(ket,"Site"));
	//	ket=prime(ket,"Site");
        //take an inner product

        Cplx eVal = eltC(ket*Op*bra);
	return eVal;
}
  auto  localEval(int j, vMPS& psi, ITensor Op, ITensor trafoTens)->double
{


  auto ket = psi.A(j);
  if(j!=length(psi))
    {ket*=psi.Ls[j];}
ket*=trafoTens;
         auto bra = dag(prime(ket,"Site"));

        auto eVal = eltC(bra*Op*ket);
	return eVal.real();
}
  
  auto  localEval(int j, MPS& psi, ITensor Op, ITensor trafoTens)->Cplx
{
  psi.position(j);

        auto ket = psi(j)*trafoTens;
        auto bra = dag(prime(ket,"Site"));
	//	ket=prime(ket,"Site");
        //take an inner product

        Cplx eVal = eltC(ket*Op*bra);
	return eVal;
}
  
  
  // void print(ITensor&  psi)
  // {
 
  //   for (int i=1; i <= length(psi); ++i) {
  //     std::cout<< i<< std::endl;
  //     Print(psi.Ls[i]);

  //   }

  //}
  template<typename T>
  struct is_vidalNot: std::false_type
  {};
  template<>
  struct is_vidalNot<vMPS> : std::true_type
  {};
 

}
#endif /* VIDALNOT2_H */
