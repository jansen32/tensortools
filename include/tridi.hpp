#ifndef TRIDI_H
#define TRIDI_H
#include "itensor/all.h"
#include <vector>
#include"mkl_lapacke.h"
//#include "itensor/tensor/ten.h"
namespace itensor{
 template<typename V>
 using Vec = Ten<VecRange,V>;
  template<typename V>
using Mat = Ten<MatRange,V>;
 template<typename V>
   struct TriDiagMat{
   TriDiagMat():diagEl(), offDiagEl() {};
   TriDiagMat(const Vec<V>& diagEl, const Vec<V>& offDiag): diagEl(diagEl), offDiagEl(offDiagEl){}
   Vec<V> diagEl;
   Vec<V> offDiagEl;
   auto dData(){return diagEl.data();}
   auto offData(){return offDiagEl.data();}
    void addElement(V DiagEl, V OffDiagEl){
      Vec<V> newDiagEl(diagEl.size()+1);
      
      Vec<V> newOffDiagEl(offDiagEl.size()+1);
      std::move(diagEl.begin(), diagEl.end(), newDiagEl.begin());
       std::move(offDiagEl.begin(), offDiagEl.end(), newOffDiagEl.begin());
       diagEl=std::move(newDiagEl);
       offDiagEl=std::move(newOffDiagEl);
       
              diagEl[diagEl.size()-1]=DiagEl;
	      offDiagEl[offDiagEl.size()-1]=OffDiagEl; 
	    
 }
    void addDiagElement(V DiagEl){
      Vec<V> newDiagEl(diagEl.size()+1);
      


      std::move(diagEl.begin(), diagEl.end(), newDiagEl.begin());
      diagEl=std::move(newDiagEl);
      diagEl[diagEl.size()-1]=DiagEl;
	    
 }   
   size_t size()const {return diagEl.size();}
   //      size_t offDiagSize()const {return offDiagEl.size();}
   V diag(size_t i) const {return diagEl[i];}
   V offDiag(size_t i) const {return offDiagEl[i];}
   ITensor make_tensor()
   {
     auto i=Index( diagEl.size(), "i");
     //auto j=Index("j", diagEl.size());
     ITensor T(i, prime(i));
     for(int l=1;l<diagEl.size(); l++)
       {
	 T.set(i(l), prime(i(l)), diagEl[l-1]);
	 T.set(i(l), prime(i(l+1)), offDiagEl[l-1]);
	 T.set(i(l+1), prime(i(l)), offDiagEl[l-1]);
	 
       }
     std::cout<< "diag l " << diagEl.size()<< std::endl;
     std::cout<< "offdiag l " << offDiagEl.size()<< std::endl;
     T.set(i(diagEl.size()), prime(i(diagEl.size())), diagEl[diagEl.size()-1]);
     return T;
   }
   
   };
// void diag(    TriDiagMat<Cplx>& tri, Vec<Cplx>& z, Vec<Cplx>& ev)
//   {
//     MKL_INT N= ev.size();
//         MKL_INT LDA= N;
//         MKL_INT info= LAPACKE_dstedc(LAPACK_COL_MAJOR, 'I', N, tri.dData(), tri.offData(), z.data(), N);
//     if(info!=0)
//       {
//   	std::cout << " diagonalization failed" << '\n';
//       }
//     ev=tri.diagEl;
//    }
  template<typename Vec, typename Mat>
  void diag(TriDiagMat<Real>& tri, Mat& z, Vec& ev)
  {
    MKL_INT N= ev.size();
        MKL_INT LDA= N;
        MKL_INT info= LAPACKE_dstedc(LAPACK_COL_MAJOR, 'I', N, tri.dData(), tri.offData(), z.data(), N);
    if(info!=0)
      {
  	std::cout << " diagonalization failed" << '\n';
      }
      ev=tri.diagEl;



   }
}
#endif /* TRIDI_H */
