#ifndef EXTRA_H
#define EXTRA_H
#include<type_traits>
#include"itensor/all.h"
namespace itensor{
  template<typename MPSType>
int
minLinkDim(MPSType const& x)
    {
    int mindim = 0;
    for( auto b : range1(length(x)-1) )
        {
        int mb = dim(linkIndex(x,b));
        mindim = std::min(mb,mindim);
        }
    return mindim;
    }
    template<typename T>
    struct use_lbo: std::false_type {
   
  };
     double step_func(const double& current_time){
     return 1./(1.+std::exp(4.-current_time));
 }
  template<typename SiteSet>
void applyCdag(MPS& psi, int i, SiteSet& sites)
{
   psi.position(i);
        psi.Aref(i)*= op(sites,"Adag",i);
	psi.Aref(i).noPrime("Site");
     for(int k = i-1; k >0; --k)
    {
      psi.position(k);
      	psi.Aref(k) *= op(sites,"F",k);
      	psi.Aref(k).noPrime("Site");
     }
  
}

 
template<typename SiteSet>
void applyC(MPS& psi, int i, SiteSet& sites)
{
                for(int k = 1; k <=i-1; ++k)
    {
      psi.position(k);
       psi.Aref(k) *= op(sites,"F",k);
      	psi.Aref(k).noPrime("Site");
     }
  	  psi.position(i);
  psi.Aref(i) *= op(sites,"A",i);
  psi.Aref(i).noPrime("Site");
  
}
  template<typename SiteSet>
void applyBdag(MPS& psi, int i, SiteSet& sites)
{
   psi.position(i);
        psi.Aref(i)*= op(sites,"Bdag",i);
	psi.Aref(i).noPrime("Site");
  
}
    template<typename SiteSet>
void applyXdag(MPS& psi, int i, SiteSet& sites)
{
   psi.position(i);
        psi.Aref(i)*= op(sites,"X",i);
	psi.Aref(i).noPrime("Site");
  
}
template<typename SiteSet>
void applyB(MPS& psi, int i, SiteSet& sites)
{
                for(int k = 1; k <=i-1; ++k)
  	  psi.position(i);
  psi.Aref(i) *= op(sites,"B",i);
  psi.Aref(i).noPrime("Site");
  
}
  template<typename SiteSet>
void applyX(MPS& psi, int i, SiteSet& sites)
{
                for(int k = 1; k <=i-1; ++k)
  	  psi.position(i);
  psi.Aref(i) *= op(sites,"X",i);
  psi.Aref(i).noPrime("Site");
  
}
    template<typename T>
  struct is_Mixed: std::false_type {
  };
    template<typename SiteSet>
  void changeBasis(SiteSet& sitesOld, SiteSet& sitesNew, MPS& psi)
  {
    for(int l=1; l<= length(sitesOld); l++)
      {


	// for now assuming 
     auto newIn=sitesNew(l);
     auto oldIn=sitesOld(l);
     auto minD=std::min(oldIn.dim(), newIn.dim());
     auto oD=oldIn.dim();
     auto nD=newIn.dim();
     auto transform=ITensor(dag(oldIn),(newIn));
     int minQdim=int(minD/2);
     transform.set(1, 1, Cplx(1, 0));
     for(int i=1; i<=minQdim; i++)
       {
	 transform.set(i, i, Cplx(1, 0));
	 transform.set(int(oD/2)+i, int(nD/2)+i, Cplx(1, 0));
       }
     psi.setA(l, psi.A(l)*transform);
     
       }
  }
template<typename BlockSparse, typename Indexable>
auto
getBlock2(BlockSparse & d,
         IndexSet const& is,
         Indexable const& block_ind)
    -> stdx::if_compiles_return<decltype(makeDataRange(d.data(),d.size())),decltype(d.offsets)>
    {
    auto r = long(block_ind.size());
    if(r == 0) return makeDataRange(d.data(),d.size());
#ifdef DEBUG
    if(is.order() != r) Error("Mismatched size of IndexSet and block_ind in getBlock");
#endif
    long ii = 0;
    for(auto i = r-1; i > 0; --i)
        {
        ii += block_ind[i];
        ii *= is[i-1].nblock();
        }
    ii += block_ind[0];
    //Do binary search to see if there
    //is a block with block index ii
    auto boff = offsetOf(d.offsets,ii);
    if(boff >= 0) return makeDataRange(d.data(),boff,d.size());
    using data_range_type = decltype(makeDataRange(d.data(),d.size()));
    return data_range_type{};
    }

}

#endif /* EXTRA_H */
